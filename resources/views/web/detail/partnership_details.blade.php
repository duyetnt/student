@extends('web.layout.master')

@section('content')

<main>
    <div id="partnership_details">
        <div class="people_details_top_title">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-12">
                            <span class="color_blue">HOME > </span><span class="color_blue">PARTNERSHIP > </span><span class="color_gray">DIPLOMATIC offices</span>
                        </div>
                    </div>
                </div>
        </div>
        <div class="content_people_details">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-12 col-12">
                        <div class="content_left_pp_details">
                            <div class="cate_pp_details">
                                <h3>Categories</h3>
                                <ul>
                                    <li>
                                        <div class="row">
                                            <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                <img src="images/squares.svg" alt="" width="60%">
                                            </div>
                                            <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                    <a href="#">Diplomatic office</a>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                            <div class="row">
                                                <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                    <img src="images/squares.svg" alt="" width="60%">
                                                </div>
                                                <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                        <a href="#">NGOS</a>
                                                </div>
                                            </div>
                                    </li>
                                    <li>
                                            <div class="row">
                                                <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                    <img src="images/squares.svg" alt="" width="60%">
                                                </div>
                                                <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                        <a href="#">SPONSORS</a>
                                                </div>
                                            </div>
                                    </li>
                                    <li>
                                            <div class="row">
                                                <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                    <img src="images/squares.svg" alt="" width="60%">
                                                </div>
                                                <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                        <a href="#">Educatiol organizations and research</a>
                                                </div>
                                            </div>
                                    </li>
                                    
                                </ul>
                            </div>
                            <div class="dif_info">
                                <div class="row">
                                        <div class="col-md-2 col-sm-2 col-2 logo_square">
                                            <img src="images/icon_square_xam.svg" alt="" width="60%">
                                        </div>
                                        <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                            <h5 class="color_yellow">Các thông tin khác</h5>
                                        </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-12">
                                        <ul>
                                                <li>
                                                    <div class="row">
                                                            <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                                <i class="fas fa-angle-double-right"></i>
                                                            </div>
                                                            <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                                <a href="#">Thông báo - Tin tức</a>
                                                            </div>
                                                    </div>
                                                </li>
                                                <li>
                                                        <div class="row">
                                                                <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                                    <i class="fas fa-angle-double-right"></i>
                                                                </div>
                                                                <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                                    <a href="#">Thời khóa biểu - Lịch thi</a>
                                                                </div>
                                                        </div>
                                                </li>
                                                <li>
                                                        <div class="row">
                                                                <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                                    <i class="fas fa-angle-double-right"></i>
                                                                </div>
                                                                <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                                    <a href="#">Mẫu đơn - Xác nhận</a>
                                                                </div>
                                                        </div>
                                                </li>
                                                <li>
                                                        <div class="row">
                                                                <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                                    <i class="fas fa-angle-double-right"></i>
                                                                </div>
                                                                <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                                    <a href="#">Học bổng</a>
                                                                </div>
                                                        </div>
                                                </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="insta_pp">
                                <div class="row">
                                        <div class="col-md-2 col-sm-2 col-2 logo_square">
                                            <img src="images/icon_square_xam.svg" alt="" width="60%">
                                        </div>
                                        <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                            <h5 class="color_yellow">Instagram</h5>
                                        </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 col-sm-4 col-4 img_insta">
                                        <img src="images/people/insta.png" alt="" width="100%">
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-4 img_insta">
                                            <img src="images/people/insta.png" alt="" width="100%">
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-4 img_insta">
                                            <img src="images/people/insta.png" alt="" width="100%">
                                    </div>
                                </div>
                            </div>
                            <div class="tag_pp">
                                <div class="row">
                                        <div class="col-md-2 col-sm-2 col-2 logo_square">
                                            <img src="images/icon_square_xam.svg" alt="" width="60%">
                                        </div>
                                        <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                            <h5 class="color_yellow">Tags</h5>
                                        </div>
                                </div>
                                <div class="row a_tag_content">
                                    <div class="col-md-12 col-sm-12 col-12">
                                        <a href="#">Cử nhân</a>
                                        <a href="#">Kinh doanh</a>
                                        <a href="#">Kế toán</a>
                                        <a href="#">Quản lý</a>
                                        <a href="#">Máy tính</a>
                                        <a href="#">Tin học</a>
                                        <a href="#">Luật</a>
                                        <a href="#">Cử nhân</a>
                                        <a href="#">Quản lý</a>
                                        <a href="#">Luật</a>
                                        <a href="#">Cử nhân</a>
                                        <a href="#">Kinh doanh</a>
                                        <a href="#">Kế toán</a>
                                        <a href="#">Quản lý</a>
                                        <a href="#">Máy tính</a>
                                        <a href="#">Tin học</a>
                                        <a href="#">Luật</a>
                                        <a href="#">Cử nhân</a>
                                        <a href="#">Quản lý</a>
                                        <a href="#">Luật</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-12 col-12">
                        <div class="content_right_pp_details">
                                <div class="row tt_right_pp_details">
                                        <div class="col-md-2 col-sm-2 col-2 logo_square">
                                            <img src="images/squares.svg" alt="" width="23%">
                                        </div>
                                        <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                            <h3 class="color_blue">Diplomatic offices</h3>
                                        </div>
                                </div>
                                <div class="row">
                                        <div class="col-md-12 col-sm-12 col-12">
                                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Cupiditate, amet, consequuntur dignissimos minima nobis a magnam facilis odio id unde sit. Rem, pariatur suscipit. Laudantium neque dolorum qui excepturi itaque.</p>
                                            <img src="images/partnership.jpg" alt="" width="100%">
                                            <p>To succeed in this era of a knowledge economy and global integration, will and enthusiasm are not enough. Learners should be equipped with scientific knowledge, standard professional skills, and proficiency in foreign languages for international integration, with the ability to work effectively on a global scale. A training institution which produces highly qualified human resources must be able to equip learners with the capacity to use those basic qualities after they graduate. Vietnam National University, Hanoi - International School(VNU- IS) is one of the higher education institutions that has been entrusted with the mission of imparting such values and has been conducting it successfully.
                                            </p>
                                        </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

@endsection