@extends('web.layout.master')

@section('content')

<main>
    <div id="living_details">
        <div class="people_details_top_title">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-12">
                            <span class="color_blue"><a href="#">HOME > </a></span><span>LIVING IN HANOI > </span><span class="color_gray">Nightlife</span>
                        </div>
                    </div>
                </div>
        </div>
        <div class="content_people_details">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-12 col-12">
                        <div class="content_left_pp_details">
                            <div class="cate_pp_details">
                                <h3>Categories</h3>
                                <ul>
                                    <li>
                                        <div class="row">
                                            <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                <img src="images/squares.svg" alt="" width="60%">
                                            </div>
                                            <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                    <a href="#">Culture Hanoi between past and future</a>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="row">
                                            <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                <img src="images/squares.svg" alt="" width="60%">
                                            </div>
                                            <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                    <a href="#">Location</a>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="row">
                                            <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                <img src="images/squares.svg" alt="" width="60%">
                                            </div>
                                            <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                    <a href="#">Traffic and transportation</a>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="row">
                                            <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                <img src="images/squares.svg" alt="" width="60%">
                                            </div>
                                            <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                    <a href="#">Nightlife</a>
                                            </div>
                                        </div>
                                    </li>
                                    
                                </ul>
                            </div>
                            <div class="insta_pp">
                                <div class="row">
                                        <div class="col-md-2 col-sm-2 col-2 logo_square">
                                            <img src="images/icon_square_xam.svg" alt="" width="60%">
                                        </div>
                                        <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                            <h5 class="color_yellow">Instagram</h5>
                                        </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 col-sm-4 col-4 img_insta">
                                        <img src="images/people/insta.png" alt="" width="100%">
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-4 img_insta">
                                            <img src="images/people/insta.png" alt="" width="100%">
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-4 img_insta">
                                            <img src="images/people/insta.png" alt="" width="100%">
                                    </div>
                                </div>
                            </div>
                            <div class="tag_pp">
                                <div class="row">
                                        <div class="col-md-2 col-sm-2 col-2 logo_square">
                                            <img src="images/icon_square_xam.svg" alt="" width="60%">
                                        </div>
                                        <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                            <h5 class="color_yellow">Tags</h5>
                                        </div>
                                </div>
                                <div class="row a_tag_content">
                                    <div class="col-md-12 col-sm-12 col-12">
                                        <a href="#">Cử nhân</a>
                                        <a href="#">Kinh doanh</a>
                                        <a href="#">Kế toán</a>
                                        <a href="#">Quản lý</a>
                                        <a href="#">Máy tính</a>
                                        <a href="#">Tin học</a>
                                        <a href="#">Luật</a>
                                        <a href="#">Cử nhân</a>
                                        <a href="#">Quản lý</a>
                                        <a href="#">Luật</a>
                                        <a href="#">Cử nhân</a>
                                        <a href="#">Kinh doanh</a>
                                        <a href="#">Kế toán</a>
                                        <a href="#">Quản lý</a>
                                        <a href="#">Máy tính</a>
                                        <a href="#">Tin học</a>
                                        <a href="#">Luật</a>
                                        <a href="#">Cử nhân</a>
                                        <a href="#">Quản lý</a>
                                        <a href="#">Luật</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-12 col-12">
                        <div class="content_right_pp_details">
                                <div class="row tt_right_pp_details">
                                        <div class="col-md-2 col-sm-2 col-2 logo_square">
                                            <img src="images/squares.svg" alt="" width="23%">
                                        </div>
                                        <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                            <h3 class="color_blue">nightlife</h3>
                                        </div>
                                </div>
                                
                                <div class="row">
                                        <div class="col-md-12 col-sm-12 col-12">
                                            <p>To succeed in this era of a knowledge economy and global integration, will and enthusiasm are not enough. Learners should be equipped with scientific knowledge, standard professional skills, and proficiency in foreign languages for international integration, with the ability to work effectively on a global scale. A training institution which produces highly qualified human resources must be able to equip learners with the capacity to use those basic qualities after they graduate. Vietnam National University, Hanoi - International School(VNU- IS) is one of the higher education institutions that has been entrusted with the mission of imparting such values and has been conducting it successfully.</p>
                                            <img src="https://sanvemaybay.com.vn/assets/uploads/2017/10/duong-pho-ha-noi-ve-dem.jpg" alt="" width="100%">
                                        </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-12">
                                        <div class="time_visit">
                                            <h5>Thời điểm để ghé thăm</h5>
                                            <p>Hầu hết du khách đến vào tháng 11-tháng 12 và tháng 3-tháng 4, khi thời tiết ấm áp và trong lành. Khí hậu nhiệt đới ẩm ướt quanh năm, với một mùa nóng và mưa từ tháng 5 đến tháng 9. Ngày lễ quan trọng bao gồm Tết, hay Tết Nguyên Đán (tháng 1/tháng 2, tùy năm); Tết trung thu/Lễ hội trăng rằm (tháng 8/tháng 9 tùy năm) và Quốc khánh (02 tháng 9) được tổ chức với pháo hoa và các cuộc đua thuyền trên hồ.</p>
                                            <div class="row">
                                                <div class="col-md-6 col-sm-6 col-6">
                                                    <div class="video_youtube_student_service">
                                                        <div class="content_video_youtube_student_service">
                                                            <iframe width="100%" height="auto" src="https://www.youtube.com/embed/Hm7Ar8YZc80" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                                        </div> 
                                                        <div class="txt_content_video_youtube">
                                                            <div class="row">
                                                                <div class="col-md-8 col-sm-8 col-8">
                                                                    <span>Rihanna - Don't Stop The Music</span>
                                                                    <span>3:54</span>
                                                                    <span>IShuffle</span>
                                                                </div>
                                                                <div class="col-md-4 col-sm-4 col-4 img_right_video_youtube">
                                                                    <img src="images/student_service/youtube.svg" alt="" width="80%">
                                                                </div>
                                                            </div>
                                                        </div>   
                                                    </div>      
                                                </div>

                                                <div class="col-md-6 col-sm-6 col-6">
                                                        <div class="video_youtube_student_service">
                                                            <div class="content_video_youtube_student_service">
                                                                    <iframe width="100%" height="auto" src="https://www.youtube.com/embed/MlCVgfI5mlA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                                            </div> 
                                                            <div class="txt_content_video_youtube">
                                                                <div class="row">
                                                                    <div class="col-md-8 col-sm-8 col-8">
                                                                        <span>Rihanna - Don't Stop The Music</span>
                                                                        <span>3:54</span>
                                                                        <span>IShuffle</span>
                                                                    </div>
                                                                    <div class="col-md-4 col-sm-4 col-4 img_right_video_youtube">
                                                                        <img src="images/student_service/youtube.svg" alt="" width="80%">
                                                                    </div>
                                                                </div>
                                                            </div>   
                                                        </div>      
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

@endsection