@extends('web.layout.master')

@section('stype.css')
    <style>
        .fix-size-image img{width: 100% !important;height: 100% !important;}
    </style>
@endsection

@section('content')
    <main>
        <div id="departments_details">
            <div class="people_details_top_title">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-12">
                            <span class="color_blue">HOME > </span><span class="color_blue">{{$menu['name']}} > </span><span
                                    class="color_gray">{{$post['name']}}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content_people_details">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-sm-12 col-12">
                            <div class="content_left_pp_details">
                                <div class="cate_pp_details">
                                    <h3>Categories</h3>
                                    <ul>
                                        @if(!empty($posts))
                                            @foreach($posts as $key => $value)
                                        <li>
                                            <div class="row">
                                                <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                    <img src="{{ asset('/public/web/images/squares.svg') }}" alt="" width="60%">
                                                </div>
                                                <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                    <a href="{{route('web.department.detail', str_slug($value['name'])."-".$value['id'])}}">{{$value['name']}}</a>
                                                </div>
                                            </div>
                                        </li>
                                            @endforeach
                                        @endif
                                    </ul>
                                </div>
                                <div class="dif_info">
                                    <div class="row">
                                        <div class="col-md-2 col-sm-2 col-2 logo_square">
                                            <img src="{{ asset('/public/web/images/icon_square_xam.svg') }}" alt="" width="60%">
                                        </div>
                                        <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                            <h5 class="color_yellow">Các thông tin khác</h5>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-12">
                                            <ul>
                                                <li>
                                                    <div class="row">
                                                        <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                            <i class="fas fa-angle-double-right"></i>
                                                        </div>
                                                        <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                            <a href="#">Thông báo - Tin tức</a>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="row">
                                                        <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                            <i class="fas fa-angle-double-right"></i>
                                                        </div>
                                                        <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                            <a href="#">Thời khóa biểu - Lịch thi</a>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="row">
                                                        <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                            <i class="fas fa-angle-double-right"></i>
                                                        </div>
                                                        <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                            <a href="#">Mẫu đơn - Xác nhận</a>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="row">
                                                        <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                            <i class="fas fa-angle-double-right"></i>
                                                        </div>
                                                        <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                            <a href="#">Học bổng</a>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="insta_pp">
                                    <div class="row">
                                        <div class="col-md-2 col-sm-2 col-2 logo_square">
                                            <img src="/public/web/images/icon_square_xam.svg" alt="" width="60%">
                                        </div>
                                        <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                            <h5 class="color_yellow">Instagram</h5>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4 col-4 img_insta">
                                            <img src="/public/web/images/people/insta.png" alt="" width="100%">
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-4 img_insta">
                                            <img src="/public/web/images/people/insta.png" alt="" width="100%">
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-4 img_insta">
                                            <img src="/public/web/images/people/insta.png" alt="" width="100%">
                                        </div>
                                    </div>
                                </div>
                                <div class="tag_pp">
                                    <div class="row">
                                        <div class="col-md-2 col-sm-2 col-2 logo_square">
                                            <img src="/public/web/images/icon_square_xam.svg" alt="" width="60%">
                                        </div>
                                        <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                            <h5 class="color_yellow">Tags</h5>
                                        </div>
                                    </div>
                                    <div class="row a_tag_content">
                                        <div class="col-md-12 col-sm-12 col-12">
                                            <a href="#">Cử nhân</a>
                                            <a href="#">Kinh doanh</a>
                                            <a href="#">Kế toán</a>
                                            <a href="#">Quản lý</a>
                                            <a href="#">Máy tính</a>
                                            <a href="#">Tin học</a>
                                            <a href="#">Luật</a>
                                            <a href="#">Cử nhân</a>
                                            <a href="#">Quản lý</a>
                                            <a href="#">Luật</a>
                                            <a href="#">Cử nhân</a>
                                            <a href="#">Kinh doanh</a>
                                            <a href="#">Kế toán</a>
                                            <a href="#">Quản lý</a>
                                            <a href="#">Máy tính</a>
                                            <a href="#">Tin học</a>
                                            <a href="#">Luật</a>
                                            <a href="#">Cử nhân</a>
                                            <a href="#">Quản lý</a>
                                            <a href="#">Luật</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-12 col-12">
                            <div class="content_right_pp_details">
                                <div class="row tt_right_pp_details">
                                    <div class="col-md-2 col-sm-2 col-2 logo_square">
                                        <img src="/public/web/images/squares.svg" alt="" width="23%">
                                    </div>
                                    <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                        <h3 class="color_blue">{{$post['name']}}</h3>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-12">
                                        <img src="{{$post['image']}}" alt="" width="100%">
                                        <div class="fix-size-image">
                                            <p>{!! $post['content'] !!}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection