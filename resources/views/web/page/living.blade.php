@extends('web.layout.master')
@section('content')
<main>
    <div id="living">
        <div class="people_top_title">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-12">
                            <a href="{{route('web.home')}}"><span class="color_blue">HOME > </span></a> <span class="color_gray">{{$menu['name']}}</span>
                        </div>
                    </div>
                </div>
        </div>
        <div class="content_people">
            <div class="container">
                <div class="row">
                    @if(!empty($posts))
                        @foreach($posts as $key => $post)
                    <div class="col-md-6 col-sm-12 col-12">
                        <div class="cate_pp_img">
                            <div class="img_cate">
                                <img src="{{$post->image}}" alt="" width="100%" height="310px" style="object-fit: cover">
                            </div>
                            <div class="txt_living">
                                <a href="{{route('web.post.detail',[$menu['code'], str_slug($post->name)."-".$post->id])}}"><h3>{{$post->name}}</h3></a>
                                <hr>
                                <p>{{$post->simulation}}</p>
                            </div>
                        </div>
                    </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
</main>
@endsection