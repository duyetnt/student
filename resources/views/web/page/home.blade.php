@extends('web.layout.master')

@section('style.css')

@endsection

@section('content')
    <main>
        <div id=home>
            <div class="banner_h">
                <div class="img_banner_h">
                    <img src="/public/web/images/banner.jpg" alt="" width="100%">
                    <div class="text_img">
                        <p class="top_text">Here you are yourself</p>
                        <p class="down_text">We career your care</p>
                    </div>
                    <div class="mark_opacity">

                    </div>
                    <div class="mark_banner">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-4 col-sm-12 col-12">
                                    <div class="img_mark_banner">
                                        <div class="img_hh">
                                            <img src="/public/web/images/home/admission.png" alt="" width="60%">
                                        </div>
                                    </div>
                                    <div class="txt_mark_banner">
                                        <h3>ADMISSION</h3>
                                        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Modi ipsa ut
                                            reprehenderit voluptatum reprehenderit</p>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-12 col-12">
                                    <div class="img_mark_banner">
                                        <div class="img_hh">
                                            <img src="/public/web/images/home/sv_service.png" alt="" width="60%">
                                        </div>
                                    </div>
                                    <div class="txt_mark_banner">
                                        <h3>STUDENT SERVICE</h3>
                                        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Modi ipsa ut
                                            reprehenderit voluptatum reprehenderit </p>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-12 col-12">
                                    <div class="img_mark_banner">
                                        <div class="img_hh">
                                            <img src="/public/web/images/home/alumni.png" alt="" width="60%">
                                        </div>
                                    </div>
                                    <div class="txt_mark_banner">
                                        <h3>Alumni</h3>
                                        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Modi ipsa ut
                                            reprehenderit voluptatum reprehenderit </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="news_home">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-12">
                            <h2>news</h2>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="test_color"></div>

            <div class="slide_home">
                <div class="container">
                    <div class="owl-carousel owl-theme slide_img_home">
                        @if(!empty($posts))
                            @foreach($posts as $key => $value)
                                <div class="item">
                                    <img src="{{ $value['image'] }}" alt="" width="80%" height="200px">
                                    <div class="title_img_slide_home"><h6>{{ $value['name'] }}</h6></div>
                                </div>
                            @endforeach
                        @endif
                        {{--<div class="item">
                            <img src="https://careers.americanexpress.com/Content/images/StudentsNew/amex_cs_internship_headers_us_undergrad.jpg" alt="" width="80%" height="200px">
                            <div class="title_img_slide_home"><h6>Work name</h6></div>
                        </div>
                        <div class="item">
                            <img src="https://careers.americanexpress.com/Content/images/StudentsNew/amex_cs_internship_headers_us_undergrad.jpg" alt="" width="80%" height="200px">
                            <div class="title_img_slide_home"><h6>Work name</h6></div>
                        </div>
                        <div class="item">
                            <img src="https://careers.americanexpress.com/Content/images/StudentsNew/amex_cs_internship_headers_us_undergrad.jpg" alt="" width="80%" height="200px">
                            <div class="title_img_slide_home"><h6>Work name</h6></div>
                        </div>--}}
                    </div>
                </div>
            </div>

            <div class="color_under_slide"></div>

            <div class="activities_home">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-12">
                            <h2>Activities</h2>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                        </div>
                    </div>
                    <div class="row h1_r">
                        <div class="mark_banner">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-4 col-sm-12 col-12">
                                        <div class="img_mark_banner">
                                            <div class="img_hh">
                                                <img src="/public/web/images/home/scholarship.png" alt="" width="50%">
                                            </div>
                                        </div>
                                        <div class="txt_mark_banner">
                                            <h3>Scholarship</h3>
                                            <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Modi ipsa ut
                                                reprehenderit voluptatum reprehenderit</p>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-12 col-12">
                                        <div class="img_mark_banner">
                                            <div class="img_hh">
                                                <img src="/public/web/images/home/job_cornor.png" alt="" width="60%">
                                            </div>
                                        </div>
                                        <div class="txt_mark_banner">
                                            <h3>Job Corner</h3>
                                            <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Modi ipsa ut
                                                reprehenderit voluptatum reprehenderit </p>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-12 col-12">
                                        <div class="img_mark_banner">
                                            <div class="img_hh">
                                                <img src="/public/web/images/home/parner_ship.png" alt="" width="60%">
                                            </div>
                                        </div>
                                        <div class="txt_mark_banner">
                                            <h3>Partnership</h3>
                                            <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Modi ipsa ut
                                                reprehenderit voluptatum reprehenderit </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row h1_r">
                        <div class="mark_banner">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-4 col-sm-12 col-12">
                                        <div class="img_mark_banner">
                                            <div class="img_hh">
                                                <img src="/public/web/images/home/featured_courses.png" alt=""
                                                     width="60%">
                                            </div>
                                        </div>
                                        <div class="txt_mark_banner">
                                            <h3>Featured courses</h3>
                                            <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Modi ipsa ut
                                                reprehenderit voluptatum reprehenderit</p>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-12 col-12">
                                        <div class="img_mark_banner">
                                            <div class="img_hh">
                                                <img src="/public/web/images/home/mobile_app.png" alt="" width="60%">
                                            </div>
                                        </div>
                                        <div class="txt_mark_banner">
                                            <h3>Mobile Apps</h3>
                                            <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Modi ipsa ut
                                                reprehenderit voluptatum reprehenderit </p>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-12 col-12">
                                        <div class="img_mark_banner">
                                            <div class="img_hh">
                                                <img src="/public/web/images/home/carees_test.png" alt="" width="60%">
                                            </div>
                                        </div>
                                        <div class="txt_mark_banner">
                                            <h3>Career Test</h3>
                                            <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Modi ipsa ut
                                                reprehenderit voluptatum reprehenderit </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="color_under_activities"></div>

            <div class="Facts_Figures_home">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-12">
                            <h2>Facts & Figures</h2>
                            <p>Số liệu về kết quả và thành tựu của sinh viên - học viên Khoa Quốc tế sau khi tốt
                                nghiệp</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-12">
                            <div class="load_percent">
                                <div class="flex-wrapper">
                                    <div class="single-chart">
                                        <svg viewBox="0 0 36 36" class="circular-chart orange">
                                            <path class="circle-bg"
                                                  d="M18 2.0845
                                        a 15.9155 15.9155 0 0 1 0 31.831
                                        a 15.9155 15.9155 0 0 1 0 -31.831"
                                            />
                                            <path class="circle"
                                                  stroke-dasharray="72, 100"
                                                  d="M18 2.0845
                                        a 15.9155 15.9155 0 0 1 0 31.831
                                        a 15.9155 15.9155 0 0 1 0 -31.831"
                                            />
                                            <text x="18" y="20.35" class="percentage">72%</text>
                                        </svg>
                                        <div class="txt_percent">
                                            <p>Nước ngoài</p>
                                        </div>
                                    </div>

                                    <div class="single-chart">
                                        <svg viewBox="0 0 36 36" class="circular-chart green">
                                            <path class="circle-bg"
                                                  d="M18 2.0845
                                        a 15.9155 15.9155 0 0 1 0 31.831
                                        a 15.9155 15.9155 0 0 1 0 -31.831"
                                            />
                                            <path class="circle"
                                                  stroke-dasharray="40, 100"
                                                  d="M18 2.0845
                                        a 15.9155 15.9155 0 0 1 0 31.831
                                        a 15.9155 15.9155 0 0 1 0 -31.831"
                                            />
                                            <text x="18" y="20.35" class="percentage">40%</text>
                                        </svg>
                                        <div class="txt_percent">
                                            <p>Tư Nhân</p>
                                        </div>
                                    </div>

                                    <div class="single-chart">
                                        <svg viewBox="0 0 36 36" class="circular-chart blue">
                                            <path class="circle-bg"
                                                  d="M18 2.0845
                                        a 15.9155 15.9155 0 0 1 0 31.831
                                        a 15.9155 15.9155 0 0 1 0 -31.831"
                                            />
                                            <path class="circle"
                                                  stroke-dasharray="90, 100"
                                                  d="M18 2.0845
                                        a 15.9155 15.9155 0 0 1 0 31.831
                                        a 15.9155 15.9155 0 0 1 0 -31.831"
                                            />
                                            <text x="18" y="20.35" class="percentage">90%</text>
                                        </svg>
                                        <div class="txt_percent">
                                            <p>Việc làm</p>
                                        </div>
                                    </div>

                                    <div class="single-chart">
                                        <svg viewBox="0 0 36 36" class="circular-chart blue">
                                            <path class="circle-bg"
                                                  d="M18 2.0845
                                            a 15.9155 15.9155 0 0 1 0 31.831
                                            a 15.9155 15.9155 0 0 1 0 -31.831"
                                            />
                                            <path class="circle"
                                                  stroke-dasharray="62, 100"
                                                  d="M18 2.0845
                                            a 15.9155 15.9155 0 0 1 0 31.831
                                            a 15.9155 15.9155 0 0 1 0 -31.831"
                                            />
                                            <text x="18" y="20.35" class="percentage">62%</text>
                                        </svg>
                                        <div class="txt_percent">
                                            <p>Nhà nước</p>
                                        </div>
                                    </div>

                                    <div class="single-chart">
                                        <svg viewBox="0 0 36 36" class="circular-chart blue">
                                            <path class="circle-bg"
                                                  d="M18 2.0845
                                            a 15.9155 15.9155 0 0 1 0 31.831
                                            a 15.9155 15.9155 0 0 1 0 -31.831"
                                            />
                                            <path class="circle"
                                                  stroke-dasharray="54, 100"
                                                  d="M18 2.0845
                                            a 15.9155 15.9155 0 0 1 0 31.831
                                            a 15.9155 15.9155 0 0 1 0 -31.831"
                                            />
                                            <text x="18" y="20.35" class="percentage">54%</text>
                                        </svg>
                                        <div class="txt_percent">
                                            <p>Start up</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="color_under_Facts_Figures_home"></div>

            <div class="slide_home_bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-12">
                            <h2>Alumni</h2>
                            <p>The success stories of the VNU-IS alumni </p>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="owl-carousel owl-theme slide_img_home">
                        @if(!empty($posts))
                            @foreach($posts as $key => $value)
                                <div class="item">
                                    <img src="{{ $value['image'] }}" alt="" width="80%" height="200px">
                                    <div class="title_img_slide_home"><h6>{{ $value['name'] }}</h6></div>
                                </div>
                            @endforeach
                        @endif
                        {{--<div class="item">
                            <img src="https://careers.americanexpress.com/Content/images/StudentsNew/amex_cs_internship_headers_us_undergrad.jpg"
                                 alt="" width="80%" height="200px">
                            <div class="title_img_slide_home"><h6>Work name</h6></div>
                        </div>
                        <div class="item">
                            <img src="https://careers.americanexpress.com/Content/images/StudentsNew/amex_cs_internship_headers_us_undergrad.jpg"
                                 alt="" width="80%" height="200px">
                            <div class="title_img_slide_home"><h6>Work name</h6></div>
                        </div>
                        <div class="item">
                            <img src="https://careers.americanexpress.com/Content/images/StudentsNew/amex_cs_internship_headers_us_undergrad.jpg"
                                 alt="" width="80%" height="200px">
                            <div class="title_img_slide_home"><h6>Work name</h6></div>
                        </div>
                        <div class="item">
                            <img src="https://careers.americanexpress.com/Content/images/StudentsNew/amex_cs_internship_headers_us_undergrad.jpg"
                                 alt="" width="80%" height="200px">
                            <div class="title_img_slide_home"><h6>Work name</h6></div>
                        </div>--}}
                    </div>
                </div>
            </div>

            <div class="form_call_book">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-sm-12 col-12">
                            <div class="call_to_action">
                                <form action="">
                                    <div class="row">
                                        <div class="col-md-8 col-sm-12 col-12">
                                            <h3>call to action</h3>
                                            <div class="input_group_call_to_action">
                                                <input type="text" class="form-control" id="usr" name="username"
                                                       placeholder="name">
                                                <input type="mail" class="form-control" id="usr" name="mail"
                                                       placeholder="email">
                                                <input type="text" class="form-control" id="usr" name="phone"
                                                       placeholder="phone">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <p class="p_4_action">Thông tin của bạn chỉ được chúng tôi sử dụng cho việc
                                                gửi thông tin từ Khoa Quốc tế tới bạn và không được tiết lộ cho bên thứ
                                                ba.</p>
                                            <button type="submit" class="btn btn-warning" value="Submit"><strong>brochure</strong><span> download</span>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12 col-12">
                            <div class="book_library">
                                <h3>book library</h3>
                                <strong class="color_blue">Using student ID</strong>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
                                <a href="#" class="color_blue">https://www.library.is.vnu.edu.vn</a>
                                <div class="button_book_library">
                                    <button type="button" class="btn btn-warning" value="Submit"><strong>find
                                            more</strong></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </main>
@endsection