@extends('web.layout.master')

@section('content')

<main>
    <div id="partnership">
        <div class="people_top_title">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-12">
                            <a href="{{route('web.home')}}">
                                <span class="color_blue">HOME > </span>
                            </a>
                            <span class="color_gray">{{$menu['name']}}</span>
                        </div>
                    </div>
                </div>
        </div>
        <div class="content_people">
            <div class="container">
                <div class="row">
                    @if(!empty($posts))
                        @foreach($posts as $key => $post)
                    <div class="col-md-6 col-sm-12 col-12">
                        <div class="cate_pp_img">
                            <div class="img_cate">
                                <img src="{{$post->image}}" alt="" width="100%">
                            </div>
                            <div class="row txt_ct_cate">
                                <div class="col-md-2 col-sm-2 col-2 logo_square">
                                    <img src="{{ asset('/public/web/images/squares.svg') }}" alt="" width="30%">
                                </div>
                                <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                    <a href="{{route('web.post.detail',[$menu['code'], str_slug($post->name)."-".$post->id])}}"><h5>{{$post->name}}</h5></a>
                                    <hr>
                                    <p>{{$post->simulation}}</p>
                                </div>
                            </div>

                        </div>
                    </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
</main>

@endsection