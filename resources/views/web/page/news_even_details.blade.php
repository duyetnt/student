@extends('web.layout.master')
@section('stype.css')
    <style>
        .fix-size-image img{width: 100% !important;height: 100% !important;}
        .txt_cate_pp_img a{
            text-transform: uppercase;
        }
    </style>
@endsection
@section('content')

    <main>
        <div id="news_even_details">
            <div class="news_even_details">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-12">
                            <a href="{{route('web.home')}}">
                                <span class="color_blue">HOME > </span>
                            </a>
                            <span class="color_gray">{{$menu['name']}}</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="content_news_even_details">
                <div class="container">
                    <div class="row">
                        <!-- phần left content -->
                        <div class="col-md-4 col-sm-12 col-12">
                            <h2>CATEGORIES</h2>
                            <ul class="nav nav-pills" id="menu">
                                @if(count($tabEvents) > 0)
                                    @foreach($tabEvents as $tabEvent)
                                        <li class="nav-item" style="text-transform: uppercase">
                                            @if($language_id == 'vi')
                                                <a class="nav_menu active" data-toggle="collapse"
                                                   href="#sub_menu_{{$tabEvent['id']}}"><img
                                                            src="/public/web/images/icon_square.png" alt=""
                                                            width="4%"><span>{{ $tabEvent['name_vi'] }}</span></a>
                                            @else
                                                <a class="nav_menu active" data-toggle="collapse"
                                                   href="#sub_menu_{{$tabEvent['id']}}">
                                                    <img src="/public/web/images/icon_square.png" alt=""
                                                                           width="4%"><span>{{ $tabEvent['name_en'] }}</span>
                                                </a>
                                            @endif
                                            <div id="sub_menu_{{$tabEvent['id']}}" class="collapse break">
                                                <ul class="nav">
                                                    @if(count($tabEvent['event_translations']) > 0)
                                                        @foreach($tabEvent['event_translations'] as $_event)
                                                            @if($language_id == $_event['language_id'])
                                                                <li class="nav-item">
                                                                    {{--<a class="nav-link" data-toggle="tab"
                                                                                        href="#sp_content_1">
                                                                        <i class="fas fa-caret-right"></i>{{$_event['name']}}
                                                                    </a>--}}
                                                                    <a href="{{route('web.event.detail', str_slug($_event['name']."-".$_event['id']))}}"><i class="fas fa-caret-right"></i>{{$_event['name']}}</a>
                                                                </li>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                </ul>
                                            </div>
                                        </li>
                                    @endforeach
                                @endif
                            </ul>


                            <div class="insta_pp">
                                <div class="row">
                                    <div class="col-md-2 col-sm-2 col-2 logo_square">
                                        <img src="/public/web/images/icon_square_xam.svg" alt="" width="40%">
                                    </div>
                                    <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                        <h5 class="color_yellow">Instagram</h5>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 col-sm-4 col-4 img_insta">
                                        <img src="/public/web/images/people/insta.png" alt="" width="100%">
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-4 img_insta">
                                        <img src="/public/web/images/people/insta.png" alt="" width="100%">
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-4 img_insta">
                                        <img src="/public/web/images/people/insta.png" alt="" width="100%">
                                    </div>
                                </div>
                            </div>

                            <div class="tag_pp">
                                <div class="row">
                                    <div class="col-md-2 col-sm-2 col-2 logo_square">
                                        <img src="images/icon_square_xam.svg" alt="" width="40%">
                                    </div>
                                    <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                        <h5 class="color_yellow">Tags</h5>
                                    </div>
                                </div>
                                <div class="row a_tag_content">
                                    <div class="col-md-12 col-sm-12 col-12">
                                        <a href="#">Cử nhân</a>
                                        <a href="#">Kinh doanh</a>
                                        <a href="#">Kế toán</a>
                                        <a href="#">Quản lý</a>
                                        <a href="#">Máy tính</a>
                                        <a href="#">Tin học</a>
                                        <a href="#">Luật</a>
                                        <a href="#">Cử nhân</a>
                                        <a href="#">Quản lý</a>
                                        <a href="#">Luật</a>
                                        <a href="#">Cử nhân</a>
                                        <a href="#">Kinh doanh</a>
                                        <a href="#">Kế toán</a>
                                        <a href="#">Quản lý</a>
                                        <a href="#">Máy tính</a>
                                        <a href="#">Tin học</a>
                                        <a href="#">Luật</a>
                                        <a href="#">Cử nhân</a>
                                        <a href="#">Quản lý</a>
                                        <a href="#">Luật</a>
                                    </div>
                                </div>
                            </div>

                        </div>


                        <!-- phần right content -->
                        <div class="col-md-8 col-sm-12 col-12">
                            <div class="tab-content">
                                @if(isset($event))

                                <div id="sp_content_1" class="container tab-pane active">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-12">
                                            <div class="main_content_news_even_details">
                                                <div class="row">
                                                    <div class="col-md-12 col-sm-12 col-12">
                                                        <div class="tt_top_main_even_details">
                                                            <div class="row">
                                                                <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                                    <img src="/public/web/images/squares.svg" alt=""
                                                                         width="18%">
                                                                </div>

                                                                <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                                    <h3 class="color_blue">{{$event['name']}}</h3>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="body_main_even_details fix-size-image">
                                                            {!! $event['content'] !!}
                                                        </div>
                                                        {{--<div class="more_info_youtube_h">
                                                            <h3>more infomation</h3>
                                                            <p>Metallica là ban nhạc thrash metal của Mỹ, thành lập ngày
                                                                28 tháng 10 năm 1981. Với hơn 100 triệu album bán ra
                                                                toàn thế giới, riêng ở Mỹ là 57 triệu album, đây là ban
                                                                nhạc heavy metal thành công nhất về mặt thương mại trong
                                                                lịch sử. Wikipedia</p>
                                                            <span>Thành viên: James Hetfield, Lars Ulrich, Kirk Hammett, THÊM</span>
                                                            <span>Các thể loại: Nhạc Kim loại Nặng, Nhạc Thrash Metal</span>
                                                            <span>Giải thưởng: Giải Grammy cho Trình diễn Metal xuất sắc nhất</span>

                                                            <div class="row">
                                                                <div class="col-md-6 col-sm-6 col-6">
                                                                    <div class="video_youtube_student_service">
                                                                        <div class="content_video_youtube_student_service">
                                                                            <iframe width="100%" height="auto"
                                                                                    src="https://www.youtube.com/embed/snNFMlFmyzg"
                                                                                    frameborder="0"
                                                                                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                                                                    allowfullscreen=""></iframe>
                                                                        </div>
                                                                        <div class="txt_content_video_youtube">
                                                                            <div class="row">
                                                                                <div class="col-md-8 col-sm-8 col-8">
                                                                                    <span>Rihanna - Don't Stop The Music</span>
                                                                                    <span>3:54</span>
                                                                                    <span>IShuffle</span>
                                                                                </div>
                                                                                <div class="col-md-4 col-sm-4 col-4 img_right_video_youtube">
                                                                                    <img src="/public/web/images/student_service/youtube.svg"
                                                                                         alt="" width="80%">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-6 col-sm-6 col-6">
                                                                    <div class="video_youtube_student_service">
                                                                        <div class="content_video_youtube_student_service">
                                                                            <iframe width="100%" height="auto"
                                                                                    src="https://www.youtube.com/embed/snNFMlFmyzg"
                                                                                    frameborder="0"
                                                                                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                                                                    allowfullscreen=""></iframe>
                                                                        </div>
                                                                        <div class="txt_content_video_youtube">
                                                                            <div class="row">
                                                                                <div class="col-md-8 col-sm-8 col-8">
                                                                                    <span>Rihanna - Don't Stop The Music</span>
                                                                                    <span>3:54</span>
                                                                                    <span>IShuffle</span>
                                                                                </div>
                                                                                <div class="col-md-4 col-sm-4 col-4 img_right_video_youtube">
                                                                                    <img src="/public/web/images/student_service/youtube.svg"
                                                                                         alt="" width="80%">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>--}}
                                                    </div>
                                                </div>

                                                <!-- more info -->
                                                {{--<div class="row">
                                                    <div class="col-md-12 col-sm-12 col-12">
                                                        <div class="more_info_youtube_h">
                                                            <h3>more infomation</h3>
                                                            <p>Metallica là ban nhạc thrash metal của Mỹ, thành lập ngày
                                                                28 tháng 10 năm 1981. Với hơn 100 triệu album bán ra
                                                                toàn thế giới, riêng ở Mỹ là 57 triệu album, đây là ban
                                                                nhạc heavy metal thành công nhất về mặt thương mại trong
                                                                lịch sử. Wikipedia</p>
                                                            <span>Thành viên: James Hetfield, Lars Ulrich, Kirk Hammett, THÊM</span>
                                                            <span>Các thể loại: Nhạc Kim loại Nặng, Nhạc Thrash Metal</span>
                                                            <span>Giải thưởng: Giải Grammy cho Trình diễn Metal xuất sắc nhất</span>

                                                            <div class="row">
                                                                <div class="col-md-6 col-sm-6 col-6">
                                                                    <div class="video_youtube_student_service">
                                                                        <div class="content_video_youtube_student_service">
                                                                            <iframe width="100%" height="auto"
                                                                                    src="https://www.youtube.com/embed/snNFMlFmyzg"
                                                                                    frameborder="0"
                                                                                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                                                                    allowfullscreen=""></iframe>
                                                                        </div>
                                                                        <div class="txt_content_video_youtube">
                                                                            <div class="row">
                                                                                <div class="col-md-8 col-sm-8 col-8">
                                                                                    <span>Rihanna - Don't Stop The Music</span>
                                                                                    <span>3:54</span>
                                                                                    <span>IShuffle</span>
                                                                                </div>
                                                                                <div class="col-md-4 col-sm-4 col-4 img_right_video_youtube">
                                                                                    <img src="/public/web/images/student_service/youtube.svg"
                                                                                         alt="" width="80%">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-6 col-sm-6 col-6">
                                                                    <div class="video_youtube_student_service">
                                                                        <div class="content_video_youtube_student_service">
                                                                            <iframe width="100%" height="auto"
                                                                                    src="https://www.youtube.com/embed/snNFMlFmyzg"
                                                                                    frameborder="0"
                                                                                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                                                                    allowfullscreen=""></iframe>
                                                                        </div>
                                                                        <div class="txt_content_video_youtube">
                                                                            <div class="row">
                                                                                <div class="col-md-8 col-sm-8 col-8">
                                                                                    <span>Rihanna - Don't Stop The Music</span>
                                                                                    <span>3:54</span>
                                                                                    <span>IShuffle</span>
                                                                                </div>
                                                                                <div class="col-md-4 col-sm-4 col-4 img_right_video_youtube">
                                                                                    <img src="/public/web/images/student_service/youtube.svg"
                                                                                         alt="" width="80%">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>--}}

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endif
                                {{--<div id="sp_content_2" class="container tab-pane">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-12">
                                            b
                                        </div>
                                    </div>
                                </div>
                                <div id="sp_content_3" class="container tab-pane">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-12">
                                            hh
                                        </div>
                                    </div>
                                </div>
                                <div id="dx_content_1" class="container tab-pane">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-12">
                                            c
                                        </div>
                                    </div>
                                </div>--}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </main>

@endsection