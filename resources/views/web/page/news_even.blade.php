@extends('web.layout.master')

@section('content')

<main>
    <div id="news_even">
        <div class="news_even_title">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-12">
                            <a href="{{route('web.home')}}">
                                <span class="color_blue">HOME > </span>
                            </a>
                            <span class="color_gray">{{$menu['name']}}</span>
                        </div>
                    </div>
                </div>
        </div>

        <div class="content_news_even">
            <div class="container">
                <div class="row">
                    @if(!empty($tabEvents))
                        @foreach($tabEvents as $tabEvent)
                    <div class="col-md-4 col-sm-12 col-12 rp_mobile_news_even">
                        <div class="col_content_h">
                            <div class="vnu_is_content">
                                <div class="top_vnu_is_content">
                                    @if($language_id == 'vi')
                                    <h5>{{$tabEvent['name_vi']}}</h5>
                                    @else
                                    <h5>{{$tabEvent['name_en']}}</h5>
                                    @endif
                                    <hr>
                                    @if($language_id == 'vi')
                                    <p>{{$tabEvent['title_vi']}}</p>
                                    @else
                                    <p>{{$tabEvent['title_en']}}</p>
                                    @endif
                                </div>
                                <div class="body_vnu_is_content">
                                    <div class="img_body_content">
                                        <img src="{{$tabEvent['image']}}" alt="" width="100%">
                                    </div>
                                    <ul>
                                        @if(count($tabEvent['event_translations']) > 0)
                                            @foreach($tabEvent['event_translations'] as $index => $event)
                                                @if($language_id == $event['language_id'])
                                        <li>
                                            <strong>EVENT {{1+$index++}}:</strong>
                                            <a href="{{route('web.event.detail', str_slug($event['name']."-".$event['id']))}}">{{$event['name']}}</a>
                                            <hr>
                                            <p>{{$event['simulation']}}</p>
                                        </li>
                                                @endif
                                            @endforeach
                                        @endif
                                    </ul>
                                </div>
                                <div class="bottom_more_vnu_is_content">
                                    <a href="#"></a>
                                </div>
                                <div class="more_content_h">
                                        <a href="#">MORE</a>
                                </div>
                            </div>
                        </div>
                    </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>

    </div>
</main>

@endsection