@extends('web.layout.master')

@section('content')

<main>
    <div id="student_service_education">
        <div class="main_ss_education">
            <div class="container-fluid">
                <div class="row">
                    <div class="banner_ss_edu">
                        <div class="img_banner_ss_edu">
                            <img src="images/huongnghiep.jpg" alt="" width="100%">
                            <div class="bg_cl_img_banner">
                            </div>
                        </div>
                        <div class="top_img_ss_edu">
                            <div class="logo_top_img_ss_edu">
                                <img src="images/logo_vang.png" alt="" width="80%">
                            </div>
                            <div class="txt_top_img_ss_edu">
                                <p><span class="color_white">Trắc Nghiệm</span>
                                    <span class="color_yellow">Hướng Nghiệp</span></p>
                                
                            </div>
                        </div>
                        <div class="img_hh_lup">
                            <img src="images/Huong nghiep.png" alt="" width="15%">
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-12">
                        <div class="content_stu_ss_edu">
                            <p class="a_content_edu">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Dolore quod soluta alias autem perspiciatis quam repellat praesentium molestiae maiores quasi voluptates numquam eos omnis officiis odit, officia ullam totam quo. Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ducimus nam beatae molestias et minima magnam! Debitis, tenetur magni. Et eaque quo omnis iste ea porro, officia perferendis! Quod, magnam iure?</p>
                            <p class="b_content_edu">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Amet ullam laudantium, numquam fuga, excepturi nesciunt tenetur minus consectetur sequi laboriosam doloribus, animi ipsum? Nemo accusantium, maxime atque exercitationem veritatis molestiae.</p>
                            <button type="button" class="btn btn-success"><span>start now</span></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

@endsection