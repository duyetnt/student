@extends('web.layout.master')
@section('content')

    <main>
        <div id="about_us">
            <div class="about_top_title">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-12">
                            <span class="color_blue">HOME > </span><span class="color_gray">about us</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="content_about_us">
                <div class="container mt-3">
                    <h2>Welcome to International School - VNU</h2>
                    <hr>
                    <div id="accordion">
                        <div class="card">
                            <div class="card-header">
                                <a class="card-link" data-toggle="collapse" href="#collapseOne">
                                    <img src="/public/web/images/squares.svg" alt="" width="1.2%"> Dean’s Message
                                </a>
                            </div>
                            <div id="collapseOne" class="collapse show" data-parent="#accordion">
                                <div class="card-body content_cosllap_one">
                                    <img src="/public/web/images/abouts/demo.jpg" alt="" width="20%">
                                    <p>A recent survey conducted by the local authorities in Phu Tan District, Ca Mau
                                        Province, about shrimp raising and wastewater treatment showed that only 53 out
                                        of 119 shrimp farm owners followed wastewater regulations. This is a common
                                        issue across Vietnam.

                                        At a meeting about shrimp raising in HCM City, Le Van Quang, deputy head of
                                        Vietnam Association of Seafood Exporters and Producers and chairman of Minh Phu
                                        Seafood Corporation, said that too many antibiotic residues were Vietnam's
                                        shrimp export weakness. Many markets including the US, Japan and the EU have
                                        tightened regulations about antibiotic residues.

                                        In the past, only 30 percent of Vietnamese shrimp cargoes were inspected, but
                                        now all of them are inspected in Japan. South Korean authorities have sent two
                                        official documents to warn over Nitrofurans residues in shrimp. The future of
                                        shrimp export to the EU is very dim and if the problem is not solved, Vietnam
                                        may lose access to those markets.

                                        "The cost for antibiotic residue testing is not cheap. Minh Phu Seafood
                                        Corporation has invested in several VND10bn (USD427,000) laboratories. As a
                                        result, the price for a kilo of shrimp increased by VND6,000 and this will make
                                        Vietnamese shrimp less competitive," Quang said.

                                        In addition, boiled shrimp from Vietnam has a pinkish colour while most markets
                                        prefer them red. "Famers often wait until the shrimp are 30-50kg to harvest.
                                        This leads to an excessive amount of shrimp of certain sizes while the markets
                                        lack other sizes," Quang said.

                                        Last year, shrimp output in India reached 600,000 tonnes. However, due to low
                                        shrimp prices in early 2018, many farmers started a new season late or switched
                                        to other jobs. The US imported 33% of Indian shrimp and has started increasing
                                        the amount of imported shrimp since September.

                                        As the world shrimp prices have started picking up, Vietnamese firms will have
                                        more chance in the US due to the trade war with China and lowered anti-dumping
                                        taxes on Vietnamese shrimp and catfish. China has also lowered taxes for seafood
                                        from ASEAN countries.

                                        Vietnam used to export 300,000 tonnes of shrimp to the US annually. However,
                                        tightened regulations from December 31 means more challenges for Vietnamese
                                        farmers and firms. Farmers are advised to co-operate with others and firms to
                                        improve shrimp quality.
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                        nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                        consequat.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card_bottom">
                            <div class="card">
                                <div class="card-header">
                                    <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">
                                        <img src="{{asset('/public/web/images/squares.svg')}}" alt="" width="1.2%">Organizationalstructure
                                    </a>
                                </div>
                                <div id="collapseTwo" class="collapse" data-parent="#accordion">
                                    <div class="card-body">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                        nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header">
                                    <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">
                                        <img src="{{asset('/public/web/images/squares.svg')}}" alt="" width="1.2%">Development
                                        Strategy
                                    </a>
                                </div>
                                <div id="collapseThree" class="collapse" data-parent="#accordion">
                                    <div class="card-body">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                        nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header">
                                    <a class="collapsed card-link" data-toggle="collapse" href="#collapseFour">
                                        <img src="images/squares.svg" alt="" width="1.2%">Mission, Vision, Core values
                                        and Motto
                                    </a>
                                </div>
                                <div id="collapseFour" class="collapse" data-parent="#accordion">
                                    <div class="card-body">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                        nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>


@endsection