@extends('web.layout.master')
@section('stype.css')
    <style>
        .fix-size-image img{width: 100% !important;height: 100% !important;}
        .txt_cate_pp_img a{
            text-transform: uppercase;
        }
    </style>
@endsection
@section('content')

    <main>
        <div id="student_service">
            <div class="student_service_top_title">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-12">
                            <a href="{{route('web.home')}}">
                                <span class="color_blue">HOME > </span>
                            </a>
                            <a href="{{route('web.menu', str_slug($menu['name']) ."-".$menu['id'])}}">
                                <span class="color_blue">{{$menu['name']}} > </span>
                            </a>
                            <span class="color_gray">@if(isset($post)){{ $post['name'] }}@elseif(isset($value)) {{ $value['name'] }} @endif</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content_people_details">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-sm-12 col-12">
                            <div class="content_left_pp_details">
                                <div class="cate_pp_details">
                                    <h3>MORE</h3>
                                    <ul>
                                        @if(!empty($posts))
                                            @foreach($posts as $key => $value)
                                                <li>
                                                    <div class="row">
                                                        <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                            <img src="{{ asset('/public/web/images/squares.svg') }}"
                                                                 alt="" width="35%">
                                                        </div>
                                                        <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                            <a href="{{ route('web.student.detail',[$menu['code'], str_slug($value->name)."-".$value->id])}}">{{ $value->name }}</a>
                                                            <?php
                                                            $date = date_create($value->created_at);
                                                            $date_create = date_format($date, "M d, Y");
                                                            ?>
                                                            <span>{{ $date_create }}</span>
                                                        </div>
                                                    </div>
                                                </li>
                                            @endforeach
                                        @endif
                                    </ul>
                                </div>
                                <div class="img_student_service">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-12">
                                            <div class="img_student_service">
                                                <img src="/public/web/images/student_service/student.jpg" alt=""
                                                     width="100%">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="video_youtube_student_service">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-12">
                                            <div class="content_video_youtube_student_service">
                                                <iframe width="100%" height="auto"
                                                        src="https://www.youtube.com/embed/snNFMlFmyzg" frameborder="0"
                                                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                                        allowfullscreen></iframe>
                                            </div>
                                            <div class="txt_content_video_youtube">
                                                <div class="row">
                                                    <div class="col-md-8 col-sm-8 col-8">
                                                        <span>Rihanna - Don't Stop The Music</span>
                                                        <span>3:54</span>
                                                        <span>IShuffle</span>
                                                    </div>
                                                    <div class="col-md-4 col-sm-4 col-4 img_right_video_youtube">
                                                        <img src="/public/web/images/student_service/youtube.svg" alt=""
                                                             width="80%">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="insta_pp">
                                    <div class="row">
                                        <div class="col-md-2 col-sm-2 col-2 logo_square">
                                            <img src="/public/web/images/icon_square_xam.svg" alt="" width="50%">
                                        </div>
                                        <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                            <h5 class="color_yellow">Instagram</h5>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4 col-4 img_insta">
                                            <img src="/public/web/images/people/insta.png" alt="" width="100%">
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-4 img_insta">
                                            <img src="/public/web/images/people/insta.png" alt="" width="100%">
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-4 img_insta">
                                            <img src="/public/web/images/people/insta.png" alt="" width="100%">
                                        </div>
                                    </div>
                                </div>
                                <div class="tag_pp">
                                    <div class="row">
                                        <div class="col-md-2 col-sm-2 col-2 logo_square">
                                            <img src="/public/web/images/icon_square_xam.svg" alt="" width="50%">
                                        </div>
                                        <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                            <h5 class="color_yellow">Tags</h5>
                                        </div>
                                    </div>
                                    <div class="row a_tag_content">
                                        <div class="col-md-12 col-sm-12 col-12">
                                            <a href="#">Cử nhân</a>
                                            <a href="#">Kinh doanh</a>
                                            <a href="#">Kế toán</a>
                                            <a href="#">Quản lý</a>
                                            <a href="#">Máy tính</a>
                                            <a href="#">Tin học</a>
                                            <a href="#">Luật</a>
                                            <a href="#">Cử nhân</a>
                                            <a href="#">Quản lý</a>
                                            <a href="#">Luật</a>
                                            <a href="#">Cử nhân</a>
                                            <a href="#">Kinh doanh</a>
                                            <a href="#">Kế toán</a>
                                            <a href="#">Quản lý</a>
                                            <a href="#">Máy tính</a>
                                            <a href="#">Tin học</a>
                                            <a href="#">Luật</a>
                                            <a href="#">Cử nhân</a>
                                            <a href="#">Quản lý</a>
                                            <a href="#">Luật</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-12 col-12">
                            <div class="content_right_pp_details">
                                <div class="row tt_right_pp_details">
                                    <div class="col-md-2 col-sm-2 col-2 logo_square">
                                        <img src="/public/web/images/squares.svg" alt="" width="13%">
                                    </div>
                                    <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                        @if(isset($post))
                                        <h3 class="color_blue">{{$post['name']}}</h3>
                                        @else
                                        <h3 class="color_blue">{{isset($value->name)? $value->name : ''}}</h3>
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-12">
                                        @if(isset($post))
                                            <img src="{{ $post['image'] }}"
                                                 alt="" width="100%">
                                            <div class="fix-size-image">
                                                <p>{!! $post['content'] !!}</p>
                                            </div>
                                        @else
                                            <img src="{{ isset($value->image)? $value->image : '' }}"
                                                 alt="" width="100%">
                                            <div class="fix-size-image">
                                                <p>{!! isset($value->content)? $value->content : '' !!}</p>
                                            </div>
                                        @endif
                                        {{--<img src="{{ $value['image'] }}"
                                             alt="" width="100%">
                                        <div class="fix-size-image">
                                            <p>{!! $value['content'] !!}</p>
                                        </div>--}}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-12">
                                        <div class="related_news_serivce">
                                            <h5>related news</h5>
                                            <div class="row">
                                                <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                    <img src="/public/web/images/squares.svg" alt="" width="13%">
                                                </div>
                                                <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                    <a href="#">Hue’s Imperial City shines bright in new promo video</a>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                    <img src="/public/web/images/squares.svg" alt="" width="13%">
                                                </div>
                                                <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                    <a href="#">Hue lists French architectural heritages for
                                                        conservation</a>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-2 col-sm-2 col-2 logo_square">
                                                    <img src="/public/web/images/squares.svg" alt="" width="13%">
                                                </div>
                                                <div class="col-md-10 col-sm-10 col-10 txt_cate_pp_img">
                                                    <a href="#">When in Hue, take a tour in the old Pagoda of the
                                                        Celestial Lady</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

@endsection