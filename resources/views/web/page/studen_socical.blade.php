@extends('web.layout.master')

@section('content')

<main>
    <div id="social">
        <div class="social_top_title">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-12">
                            <a href="{{route('web.home')}}"><span class="color_blue">HOME > </span></a> <span class="color_gray">{{$menu['name']}}</span>
                        </div>
                    </div>
                </div>
        </div>
        
        <div class="main_soial">
            <div class="container">
                <div class="row">
                    @if(!empty($networks))
                        @foreach($networks as $key => $network)
                    <div class="col-md-4 col-sm-12 col-12">
                        <div class="content_main_h">
                            <a href="{{ $network['link'] }}" target="_blank">
                                <div class="img_content_main_h">
                                        <img src="{{$network['image']}}" alt="" width="100%" height="350">
                                        <div class="mark_img_social">
                                        </div>
                                        <div class="img_icon_social">
                                            <img src="{{ $network['icon'] }}" alt="" width="50%">
                                        </div>
                                </div>
                            </a>
                        </div>
                    </div>
                        @endforeach
                    @endif
                    {{--<div class="col-md-4 col-sm-12 col-12">
                            <div class="content_main_h">
                                <a href="#">
                                    <div class="img_content_main_h">
                                        <img src="http://c0.img.chungta.vn/2017/07/18/f1-7730-1465809197-5416-1500388016.jpg" alt="" width="100%" height="350">
                                        <div class="mark_img_social">
                                        </div>
                                        <div class="img_icon_social">
                                            <img src="/public/web/images/social/h6.png" alt="" width="50%">
                                        </div>
                                    </div>
                                </a>
                            </div>
                    </div>
                    <div class="col-md-4 col-sm-12 col-12">
                            <div class="content_main_h">
                                <a href="#">
                                    <div class="img_content_main_h">
                                        <img src="https://www.hutech.edu.vn/imgnews/homepage/stories/hinh209-ssg/nghi_le.jpg" alt="" width="100%" height="350">
                                        <div class="mark_img_social">
                                        </div>
                                        <div class="img_icon_social">
                                            <img src="/public/web/images/social/h5.png" alt="" width="50%">
                                        </div>
                                    </div>
                                </a>
                            </div>
                    </div>
                    <div class="col-md-4 col-sm-12 col-12">
                            <div class="content_main_h">
                                <a href="#">
                                    <div class="img_content_main_h">
                                        <img src="http://giaiphapdaotaovnnp.edu.vn/images/chuy%E1%BB%87n-sinh-vi%C3%AAn.jpg" alt="" width="100%" height="350">
                                        <div class="mark_img_social">
                                        </div>
                                        <div class="img_icon_social">
                                            <img src="/public/web/images/social/h2.png" alt="" width="50%">
                                        </div>
                                    </div>
                                </a>
                            </div>
                    </div>
                    <div class="col-md-4 col-sm-12 col-12">
                            <div class="content_main_h">
                                <a href="#">
                                    <div class="img_content_main_h">
                                        <img src="http://hanoimoi.com.vn/Uploads/trieuhoa/2015/10/1/ql_perth.jpg" alt="" width="100%" height="350">
                                        <div class="mark_img_social">
                                        </div>
                                        <div class="img_icon_social">
                                            <img src="/public/web/images/social/h3.png" alt="" width="50%">
                                        </div>
                                    </div>
                                </a>
                            </div>
                    </div>
                    <div class="col-md-4 col-sm-12 col-12">
                        <a href="#">
                            <div class="content_main_h">
                                <div class="img_content_main_h">
                                    <img src="http://dantricdn.com/Uploaded/2009/12/16/sinh-vien16122009.jpg" alt="" width="100%" height="350">
                                    <div class="mark_img_social">
                                    </div>
                                    <div class="img_icon_social">
                                        <img src="/public/web/images/social/h4.png" alt="" width="50%">
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>--}}
                </div>
            </div>
        </div>

    </div>
</main>

@endsection