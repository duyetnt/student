@extends('web.layout.master')
@section('content')

    <main>
        <div id="about_us">
            <div class="about_top_title">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-12">
                            <a href="{{route('web.home')}}">
                                <span class="color_blue">HOME > </span>
                            </a>
                            <span class="color_gray">{{$menu['name']}}</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="content_about_us">
                <div class="container mt-3">
                    <h2>Welcome to International School - VNU</h2>
                    <hr>
                    <div id="accordion">
                        <div class="card_bottom">
                            @foreach($posts as $key => $post)
                            <div class="card">
                                <div class="card-header">
                                    <a class="card-link" data-toggle="collapse" href="#collapseOne-{{$key}}">
                                        <img src="/public/web/images/squares.svg" alt="" width="1.2%"> {{$post->name}}
                                    </a>
                                </div>
                                <div id="collapseOne-{{$key}}" class="{{$key == 0 ? 'collapse show': 'collapse'}}" data-parent="#accordion">
                                    <div class="card-body content_cosllap_one">
                                        {!! $post->content !!}
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>


@endsection