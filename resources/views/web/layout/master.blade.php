<!DOCTYPE html>
<html>
<head>
    <title>INTERNATIONAL SCHOOL</title>
    
    <base href="{{url('/web')}}/">
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="icon" href="https://is2-ssl.mzstatic.com/image/thumb/Purple128/v4/85/67/b1/8567b1e5-70a0-3e16-f67d-d13892d82f19/source/512x512bb.jpg">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/css?family=Montserrat|Oswald|Josefin+Sans" rel="stylesheet">
	
	<link rel="stylesheet" type="text/css" href="{{asset('/public/web/css/style.css')}}">
	@yield('stype.css')
</head>
<body>

	@include('web.layout.header')
	
	<!-- main -->
	@yield('content')
	<!-- end main -->

	<!-- footer -->
	@include('web.layout.footer')
	<!-- end footer -->

	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
	<script type="text/javascript" src="{{asset('/public/web/js/index.js')}}"></script>
</body>
</html>