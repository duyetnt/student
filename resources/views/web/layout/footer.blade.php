<footer>
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-12 col-12">
                    <div class="about_us_footer">
                        <h3>About us</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>

                        <div class="socical_about_us_footer">
                            <ul>
                                <li><a href="#"><img src="images/home/logo1.png" alt="" width="80%"></a></li>
                                <li><a href="#"><img src="images/home/logo2.png" alt="" width="80%"></a></li>
                                <li><a href="#"><img src="images/home/logo3.png" alt="" width="80%"></a></li>
                                <li><a href="#"><img src="images/home/logo4.png" alt="" width="80%"></a></li>
                                <li><a href="#"><img src="images/home/logo5.png" alt="" width="80%"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-12 col-12">
                    <div class="quick_link_footer">
                        <h3>quick links</h3>
                        <ul>
                            <li><a href="#"><i class="fas fa-angle-right color_yellow"></i>Tuyển sinh Đại học</a></li>
                            <li><a href="#"><i class="fas fa-angle-right color_yellow"></i>Tuyển sinh Sau Đại học</a></li>
                            <li><a href="#"><i class="fas fa-angle-right color_yellow"></i>Công tác HSSV</a></li>
                            <li><a href="#"><i class="fas fa-angle-right color_yellow"></i>Alumni</a></li>
                            <li><a href="#"><i class="fas fa-angle-right color_yellow"></i>Q&A</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-5 col-sm-12 col-12">
                    <div class="get_in_touch_footer">
                        <h3>get in touch</h3>
                        <div class="home_get_in_touch_footer">
                            <div class="row">
                                <div class="col-md-1">
                                    <i class="fas fa-home color_yellow"></i>	
                                </div>
                                <div class="col-md-11">
                                    <div class="content_right_home_get_in_touch_footer">
                                            <div class="row">
                                                    <div class="col-md-12">
                                                        <span><strong class="color_yellow">Cơ sở 1:</strong> Nhà G7 & G8, 144 Xuân Thủy, Cầu Giấy, Hà Nội</span>
                                                    </div>
                                            </div>
                                            <div class="row">
                                                    <div class="col-md-12">
                                                        <span><strong class="color_yellow">Cơ sở 2:</strong> Nhà C, nhà 21 tầng và nhà E Làng sinh viên HACINCO, 99 Ngụy Như Kon Tum, Nhân Chính, Thanh Xuân, Hà Nội</span>
                                                    </div>
                                            </div>
                                            <div class="row">
                                                    <div class="col-md-12">
                                                        <span><strong class="color_yellow">Cơ sở 3:</strong> Phố Kiều Mai, Phường Phúc Diễn, Quận Bác Từ Liêm, Hà Nôi</span>
                                                    </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="phone_get_in_touch_footer">
                            <div class="row">
                                <div class="col-md-1">
                                    <i class="fas fa-phone-volume color_yellow"></i>
                                </div>
                                <div class="col-md-11">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="phone_left_phone_get_in_touch_footer">
                                                <p>+84 (024) 3754 8065</p>
                                                <p>+84 (024) 3557 5992</p>
                                            </div>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="phone_right_phone_get_in_touch_footer">
                                                <p><strong class="color_yellow">Đại học: </strong>0983 372 988 | 0379 884 488</p>
                                                <p><strong class="color_yellow">Sau đại học: </strong>0932 323 252</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <div class="mail_get_in_touch_footer">
                            <div class="row">
                                <div class="col-md-1">
                                    <i class="fas fa-envelope color_yellow"></i>
                                </div>
                                <div class="col-md-11">
                                        <span>khoaquocte@is.vnu.vn</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>