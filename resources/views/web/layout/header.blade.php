	<!-- header -->
	<?php
	 $menus = \App\Models\Admin\Menu::orderBy('stt', 'asc')->get()->toArray();
	?>
	<header>
		<div class="top_header">
			<div class="container-fluid">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-12">
							<div class="container">
									<div class="main_header_content">
										<div class="left_content_header">
											<p class="color_yellow">Have any questions?</p>
											<p><i class="fas fa-phone-volume color_yellow"></i><a href="#" class="color_white">0983 372 988</a><span class="color_yellow"> | </span><a href="#" class="color_white">01679 884 488</a></p>
											<p class="color_white"><i class="far fa-envelope color_yellow"></i>sinhvien.khoaquocte@gmail.com</p>
										</div>
										<div class="login_header">
											<a href="#" class="color_blue">Register</a>
											<span class="color_blue">or</span>
											<a href="#"class="color_blue">Login</a>
											<p class="color_yellow"><a href="#" class="color_yellow">using student ID</a></p>
										</div>
									</div>
							</div>
							
							<div class="language_header">
								<a href="{{route('web.language', 'vietnam')}}" class="color_yellow">Tiếng Việt</a>
								<a href="{{route('web.language', 'english')}}" class="color_white">English</a>
							</div>
						</div>
					</div>
			</div>
		</div>
		

	<!-- menu chính -->
		<div class="menu_h">
				<div class="container">
					<div class="row">
						<div class="col-md-4 col-sm-12 col-12">
							<div class="logo_h">
								<a href="{{route('web.home')}}">
									<img src="/public/web/images/logo.png" alt="" width="75%">
								</a>
							</div>
						</div>
						<div class="col-md-8 col-sm-12 col-12">
							<div class="main_menu">
								<ul>
									@foreach($menus as $key => $menu)
										<li><a href="{{route('web.menu', str_slug($menu['name']) ."-".$menu['id'])}}">{{$menu['name']}}</a><span class="color_yellow"> | </span></li>
									@endforeach

									{{--<li><a href="{{route('abouts')}}">ABOUT US</a><span class="color_yellow"> | </span></li>
									<li><a href="{{route('people')}}">PEOPLE</a><span class="color_yellow"> | </span></li>
									<li><a href="{{route('departments')}}">DEPARTMENTS</a><span class="color_yellow"> | </span></li>
									<li><a href="{{route('partnership')}}">PARTNERSHIPS</a><span class="color_yellow"> | </span></li>
									<li><a href="{{route('programs')}}">PROGRAMS</a><span class="color_yellow"></span></li>
									<li><a href="{{route('student_service')}}">STUDENT SERVICES</a><span class="color_yellow"> | </span></li>
									<li><a href="{{route('news_even')}}">News & Events</a><span class="color_yellow"> | </span></li>
									<li><a href="{{route('living')}}">LIVING IN HANOI</a><span class="color_yellow"> | </span></li>
									<li><a href="{{route('abouts')}}">VNU-ISOCIAL</a><span class="color_yellow"></span></li>--}}
								</ul>
							</div>
							<form action="" class="form_search"> 
									<div class="input-group mb-3">
											<input type="text" class="form-control" placeholder="Keyword search">
											<div class="input-group-append">
											<button class="btn btn-success" type="submit">Search</button>  
											</div>
									</div>
							</form>
						</div>
					</div>
				</div>
		</div>
	<!-- end menu chính -->

	<!-- menu nhỏ bên phải -->
		<div class="menu_right">
				<div class="tool_icon_right tool_show_txt" style="z-index: 3;">
				<span class="trigger"></span>
				<div class="inner">
					<p class="item item_border item_a">
						<a class="toolAction" href="#"><i class="fab fa-instagram"></i></a>
					</p>
					<p class="item item_border item_b">
						<a class="toolAction" href="#"><img src="/public/web/images/home/subiz.png" alt="" width="70%"></a>
					</p>
					<p class="item item_border item_a">
						<a class="toolAction" href="#"><i class="fab fa-facebook-f"></i></a>
					</p>
					<p class="item item_border item_b">
						<a href="#" target="_blank"><img src="/public/web/images/home/youtube.png" alt="" width="60%"></a>
					</p>
					<p class="item item_border item_a">
						<a href="#" target="_blank"><i class="fab fa-linkedin-in"></i></a>
					</p>
					<p class="item item_border item_b">
						<a class="toolAction" href="#"><img src="/public/web/images/home/zalo.png" alt="" width="60%"></a>
					</p>
					<p class="item item_border item_a">
						<a href="#" target="_blank"><i class="far fa-envelope"></i></a>
					</p>
					<p class="item item_border item_b">
						<a href="#" target="_blank"><img src="/public/web/images/home/printer.svg" alt="" width="50%"></a>
					</p>
					<p class="item get_info item_a">
						<a href="#" target="_blank">nhận thông tin từ chúng tôi</a>
					</p>
				</div>
				</div>
		</div>
	<!-- end menu nhỏ bên phải -->

	</header>
	<!-- end header -->