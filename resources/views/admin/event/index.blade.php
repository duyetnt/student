@extends('admin.layout.master')
@section('style.css')
    {{----}}
@endsection
@section('content')
    <?php
    $language = \App\Models\Admin\Language::getLanguage();
    $menu = \App\Models\Admin\Menu::getAllMenu();
    $eventTrans = \App\Models\Admin\EventTranslation::getAllEvent();
    //dd($language);
    ?>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Gemini</h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Data Table With Full Features</h3>
                            @if (session('success'))
                                <div class="alert alert-success pull-right alert-dismissable margin5" style="width: 50%; text-align: center">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    {{ session('success') }}
                                </div>
                            @endif
                            @if (session('error'))
                                <div class="alert alert-danger pull-right alert-dismissable margin5" style="width: 50%; text-align: center">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    {{ session('error') }}
                                </div>
                            @endif
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th class="col-md-1">STT</th>
                                    <th class="col-md-3">Tên sản phẩm</th>
                                    <th class="col-md-4">Mô phỏng</th>
                                    <th class="col-md-2">Thuộc menu</th>
                                    <th class="col-md-2">
                                        <a href="{{route('admin.event.create')}}" type="button" class="btn btn-block btn-info">
                                            Thêm Mới
                                            <i class="fa fa-fw fa-plus-square"></i>
                                        </a>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(!empty($events))
                                    @foreach($events as $key => $event)
                                <tr>
                                    <td>{{1+$key++}}</td>
                                    <td>{{ $eventTrans['name'][$event['id']] }}</td>
                                    <td>{{ str_limit($eventTrans['simulation'][$event['id']],100) }}</td>
                                    <td>{{ $menu[$eventTrans['menu'][$event['id']]]}}</td>
                                    <td>
                                        @if(count($event['event_translations']) == 2)
                                            @foreach($event['event_translations'] as $value)
                                                @if($language[$value['language_id']] == 'english')
                                        &nbsp;<a href="{{route('admin.event.edit', $value['id'])}}"><img src="/public/upload/icon/english.png" width="30"></a>&nbsp;
                                                @else
                                                    &nbsp;<a href="{{route('admin.event.edit', $value['id'])}}"><img src="/public/upload/icon/vietnam.png" width="30"></a>
                                                @endif
                                            @endforeach
                                        @else

                                            @foreach($event['event_translations'] as $value)
                                                @if($language[$value['language_id']] == 'english')
                                                    &nbsp;<a href="{{route('admin.event.edit', $value['id'])}}"><img src="/public/upload/icon/english.png" width="30"></a>&nbsp;
                                                    &nbsp;<a href="{{route('admin.event.create.lang', [$event['id'],$value['event_tab_id'],$value['menu_id'],'vietnam'])}}"><img src="/public/upload/icon/vietnam.png" width="30"></a>&nbsp;
                                                @elseif($language[$value['language_id']] == 'vietnam')
                                                    &nbsp;<a href="{{route('admin.event.edit', $value['id'])}}"><img src="/public/upload/icon/vietnam.png" width="30"></a>&nbsp;
                                                    &nbsp;<a href="{{route('admin.event.create.lang', [$event['id'],$value['event_tab_id'],$value['menu_id'],'english'])}}"><img src="/public/upload/icon/english.png" width="30"></a>
                                                @endif
                                            @endforeach
                                            @endif

                                        {{--<a href="{{route('admin.post.edit', $event['id'])}}" type="button" class="btn btn-info">
                                            <i class="fa fa-edit"></i>
                                        </a>--}}
                                        <button type="button" class="btn btn-danger btn-delete" data-toggle="modal"
                                                data-target="#modal-delete-{{$event['id']}}">
                                            <i class="fa fa-fw fa-trash-o"></i>
                                        </button>
                                        <div class="modal modal-danger fade" id="modal-delete-{{$event['id']}}">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title">Bạn có chắc chắn muốn xóa bài viết này ?</h4>
                                                    </div>
                                                    <div class="modal-body" style="background: white!important;">
                                                        <form method="get" action="{{route('admin.post.destroy', $event['id'])}}">
                                                            <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Hủy bỏ
                                                                <i class="fa fa-fw fa-close"></i>
                                                            </button>
                                                            <button type="submit" class="btn btn-success">Xác nhận
                                                                <i class="fa fa-save"></i>
                                                            </button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </td>
                                </tr>
                                @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

    {{--<div class="show-modal">
        <div class="modal modal-danger fade" id="modal-delete">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Bạn có chắc chắn muốn tài khoản này ?</h4>
                    </div>
                    <div class="modal-body" style="background: white!important;">
                        <form method="post" action="{{route('admin.post.destroy')}}">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <input type="hidden" name="id" id="data-id">
                            <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Hủy bỏ
                                <i class="fa fa-fw fa-close"></i>
                            </button>
                            <button type="submit" class="btn btn-success">Xác nhận
                                <i class="fa fa-save"></i>
                            </button>
                        </form>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    </div>--}}
@endsection
@section('javascript')
    {{----}}
    <script>
        $(document).ready(function () {
            $('.alert-dismissable').delay(5000).slideUp();
            $('.btn-delete').click(function () {
                var id = $(this).attr('data-id');
                $('#data-id').val(id);
            });
        });
    </script>
@endsection