@extends('admin.layout.master')

@section('content')
    

<div class="container">
    <h2>Quản lý thông tin chung website</h2>           
    <table class="table">
      <thead>
        <tr>
          <th class="col-md-2">Tên thông tin</th>
          <th class="col-md-3">Vị trí</th>
          <th class="col-md-4">giá trị</th>
          <th class="col-md-3">action</th>
        </tr>
      </thead>
      <tbody>
            @foreach($caidat as $cd)
            <tr>
                <td>{{ $cd->description }}</td>
                <td>{{ $cd->position }}</td>
                <td>{{ $cd->value }}</td>
                <td><button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-default">
                       Sửa
                      </button></td>
            </tr>
            @endforeach
      </tbody>
    </table>
    <div class="modal fade" id="modal-default" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Default Modal</h4>
            </div>
            <div class="modal-body">
                <form action="" method="post">
                        <input type="text" class="form-control" id="usr" name="value">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>
   
    



@endsection