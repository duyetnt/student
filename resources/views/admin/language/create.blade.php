@extends('admin.layout.master')

@section('style.css')
    {{----}}
@endsection

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Gemini</h1>
        </section>

        <!-- Main content -->
        <section class="content">

            <form class="form-horizontal" action="{{route('admin.language.store')}}" method="post">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="form-group">
                    <label class="control-label col-sm-3" for="email">Tên danh mục(*):</label>
                    <div class="col-sm-8">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-fw fa-file-text-o"></i></span>
                            <input type="text" class="form-control" name="name" placeholder="Nhập tên danh mục" value="{{ old('name') }}" required>
                        </div>
                        @if($errors->has('name'))
                            <span class="text-center text-danger" role="alert">
                                {{$errors->first('name')}}
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-8">
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-save"></i>Thêm mới
                        </button>
                        <a href="{{route('admin.language.list')}}" type="submit" class="btn btn-danger">
                            <i class="fa fa-fw fa-close"></i>Hủy Bỏ
                        </a>
                    </div>
                </div>
            </form>

        </section>
        <!-- /.content -->
    </div>
@endsection

@section('javascript')
    {{----}}
    <script>
        $(document).ready(function () {
            $('.alert-dismissable').delay(5000).slideUp();
        });
    </script>
@endsection