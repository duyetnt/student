@extends('admin.layout.master')

@section('style.css')
    {{----}}
    <style>
        .percent{display: none}
        .cke_dialog_tabs{display: none!important;}
    </style>
@endsection

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                @if($language == 'english')
                    Bài viết tiếng Anh
                    @else
                Bài viết tiếng việt
                    @endif
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">

            <form class="form-horizontal" action="{{route('admin.post.store.lang')}}" method="post" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <input type="hidden" name="language_id" value="{{$language}}">
                <input type="hidden" name="menu_id" value="{{$menu_id}}">
                <input type="hidden" name="post_id" value="{{$post_id}}">

                <div class="form-group">
                    <label class="control-label col-sm-3" for="email">Tên bài viết(*):</label>
                    <div class="col-sm-8">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-fw fa-file-text-o"></i></span>
                            <input type="text" class="form-control" name="name" placeholder="Tên bài viết" value="{{ old('name') }}">
                        </div>
                        @if($errors->has('name'))
                            <span class="text-center text-danger" role="alert">
                                {{$errors->first('name')}}
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-3" for="email">Mô phỏng bài viết(*):</label>
                    <div class="col-sm-8">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-fw fa-file-text-o"></i></span>
                            <textarea type="text" class="form-control" name="simulation" placeholder="Mô phỏng bài viết" value="">{{ old('simulation') }}</textarea>
                        </div>
                        @if($errors->has('simulation'))
                            <span class="text-center text-danger" role="alert">
                                {{$errors->first('simulation')}}
                            </span>
                        @endif
                    </div>
                </div>

                <div id="image-diplay">
                    <div class="form-group">
                        <label class="control-label col-md-3">Chọn ảnh hiển thị bài viết(*):</label>
                        <div class="col-md-8">
                            <div class="fileinput fileinput-new" data-provides="fileinput" style="display:unset!important;">
                                <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                                     style="width: 100%; height: 250px;">

                                </div>
                                <div>
                                    <span class="btn red btn-outline btn-file" style="background: #1f81ff !important;">
                                        <span class="fileinput-new"> Chọn ảnh </span>
                                        <span class="fileinput-exists"> Đổi ảnh </span>
                                        <input type="file" name="image">
                                    </span>
                                    <a style="background: #d21717 !important;color: white" href="javascript:;" class="btn red fileinput-exists"
                                       data-dismiss="fileinput"> Xóa ảnh </a>
                                    {{--<button type="button" class="btn btn-success add-image">Thêm ảnh</button>--}}
                                </div>
                                <span class="text-center text-danger" role="alert">
                                    @if($errors->has('image'))
                                        {{$errors->first('image')}}
                                    @endif
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <hr style="border: 1px solid black">
                <label class="control-label col-sm" for="email">Nội dung bài viết(*):</label><br><br>
                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="input-group">
                            <textarea type="text" class="form-control editor1" name="content">{!! old('content') !!}</textarea>
                            @if($errors->has('content'))
                                <span class="text-center text-danger" role="alert">
                                    {{$errors->first('content')}}
                                </span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-8">
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-save"></i>
                            Thêm Mới</button>
                        <a href="{{route('admin.post.list')}}" type="submit" class="btn btn-danger">
                            <i class="fa fa-fw fa-close"></i>Hủy Bỏ
                        </a>
                    </div>
                </div>
            </form>

        </section>
        <!-- /.content -->
    </div>
@endsection

@section('javascript')
    <script src="{{ asset('public/pulgin/ckeditor/ckeditor.js') }}"></script>
    {{----}}
    <script>
        CKEDITOR.replaceClass = 'editor1';
        $(document).ready(function () {
            $('.alert-dismissable').delay(5000).slideUp();
            $('body').on('click', '.add-image', function () {
                var html ='';
                html =
                    '<div class="form-group">'
                    +'<label class="control-label col-md-3"></label>'
                    +'<div class="col-md-8">'
                    +'<div class="fileinput fileinput-new" data-provides="fileinput" style="display:unset!important;">'
                    +'<div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 100%; height: 250px;">'
                    +'</div>'
                    +'<div>'
                    +'<span class="btn red btn-outline btn-file" style="background: #1f81ff !important;">'
                    +'<span class="fileinput-new"> Chọn ảnh </span>'
                    +'<span class="fileinput-exists"> Đổi ảnh </span>'
                    +'<input type="file" name="images[]" multiple>'
                    +'</span>'
                    +'<button type="button" class="btn btn-danger delete-image">Xóa bỏ</button>'
                    +'<button type="button" class="btn btn-success add-image">Thêm ảnh</button>'
                    +'</div>'
                    +'</div>'
                    +'</div>'
                    +'</div>';
                $("#image-diplay").append(html);
            });
            $('body').on('click', '.delete-image', function () {
                $(this).parent().parent().parent().remove();
            });
        });
    </script>
@endsection