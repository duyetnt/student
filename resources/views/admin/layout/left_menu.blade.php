<ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>

        <li class="treeview">
            <a href="#">
                <i class="fa fa-laptop"></i>
                <span>Danh Mục</span>
                <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="pages/UI/general.html"><i class="fa fa-circle-o"></i> Danh sách danh mục</a></li>
                <li><a href="pages/UI/icons.html"><i class="fa fa-circle-o"></i> Thêm danh mục</a></li>
                <li><a href="pages/UI/buttons.html"><i class="fa fa-circle-o"></i> Sửa danh mục</a></li>
            </ul>
        </li>
        {{--the menu--}}
        <li class="treeview">
            <a href="#">
                <i class="fa fa-laptop"></i>
                <span>QL Menu</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                    </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="{{route('admin.menu.list')}}"><i class="fa fa-circle-o"></i> Danh sách Menu</a></li>
                <li><a href="{{route('admin.menu.create')}}"><i class="fa fa-circle-o"></i> Thêm Menu</a></li>
            </ul>
        </li>
        {{--end menu--}}
        {{--category--}}
        <li class="treeview">
            <a href="#">
                <i class="fa fa-laptop"></i>
                <span>QL danh mục</span>
                <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                        </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="{{route('admin.menu.list')}}"><i class="fa fa-circle-o"></i> Danh sách danh mục</a></li>
                <li><a href="{{route('admin.menu.create')}}"><i class="fa fa-circle-o"></i> Thêm danh mục</a></li>
            </ul>
        </li>
        {{--end category--}}
        {{--the language--}}
        <li class="treeview">
            <a href="#">
                <i class="fa fa-laptop"></i>
                <span>QL Language</span>
                <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                        </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="{{route('admin.language.list')}}"><i class="fa fa-circle-o"></i> Danh sách Language</a></li>
                <li><a href="{{route('admin.language.create')}}"><i class="fa fa-circle-o"></i> Thêm Language</a></li>
            </ul>
        </li>
        {{--end language--}}
        {{--the language--}}
        <li class="treeview">
            <a href="#">
                <i class="fa fa-laptop"></i>
                <span>QL bài viết</span>
                <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                            </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="{{route('admin.post.list')}}"><i class="fa fa-circle-o"></i> Danh sách bài viết</a></li>
                <li><a href="{{route('admin.post.create')}}"><i class="fa fa-circle-o"></i> Thêm bài viết</a></li>
            </ul>
        </li>
        {{--end language--}}
        {{--event--}}
        <li class="treeview">
            <a href="#">
                <i class="fa fa-laptop"></i>
                <span>QL sự kiện</span>
                <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                                </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="{{route('admin.event.list')}}"><i class="fa fa-circle-o"></i> Danh sách sự kiện</a></li>
                <li><a href="{{route('admin.event.create')}}"><i class="fa fa-circle-o"></i> Thêm sự kiện</a></li>
                <li><a href="{{route('admin.tab_event.list')}}"><i class="fa fa-circle-o"></i> QL tab sự kiện</a></li>
            </ul>
        </li>
        {{--end event--}}


        <li class="treeview">
            <a href="#">
                <i class="fa fa-laptop"></i>
                <span>Cài đặt</span>
                <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="{{ route('caidat') }}"><i class="fa fa-circle-o"></i> Quản lý logo</a></li>
                <li><a href="{{ route('caidat') }}"><i class="fa fa-circle-o"></i> Quản lý số điện thoại</a></li>
                <li><a href="{{ route('caidat') }}"><i class="fa fa-circle-o"></i> Quản lý Email</a></li>
                <li><a href="{{ route('caidat') }}"><i class="fa fa-circle-o"></i> Quản lý địa chỉ cơ sở</a></li>
            </ul>
        </li>

        <li class="treeview">
            <a href="#">
                <i class="fa fa-laptop"></i>
                <span>QL kết nối</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                    </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="{{ route('admin.network.list') }}"><i class="fa fa-circle-o"></i> Quản lý kết nối</a></li>
                <li><a href="{{ route('admin.network.create') }}"><i class="fa fa-circle-o"></i> Thêm mới kết nối</a></li>
            </ul>
        </li>

      </ul>