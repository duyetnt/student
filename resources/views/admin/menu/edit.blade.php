@extends('admin.layout.master')

@section('style.css')
    {{----}}
@endsection

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            @if (session('error'))
                <div class="alert alert-danger pull-right alert-dismissable margin5" style="width: 100%; text-align: center">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ session('error') }}
                </div>
            @endif
        </section>

        <!-- Main content -->
        <section class="content">
            <form class="form-horizontal" action="{{route('admin.menu.update', $menu['id'])}}" method="post">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="form-group">
                    <label class="control-label col-sm-3" for="email">Tên danh mục(*):</label>
                    <div class="col-sm-8">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-fw fa-file-text-o"></i></span>
                            <input type="text" class="form-control" name="name" placeholder="Nhập tên danh mục" value="{{ $menu['name'] }}" required>
                        </div>
                        @if($errors->has('name'))
                            <span class="text-center text-danger" role="alert">
                                {{$errors->first('name')}}
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-3" for="email">Tiêu đề(*):</label>
                    <div class="col-sm-8">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-fw fa-file-text-o"></i></span>
                            <input type="text" class="form-control" name="title" placeholder="Nhập tiêu đề danh mục" value="{{ $menu['title'] }}" required>
                        </div>
                        @if($errors->has('title'))
                            <span class="text-center text-danger" role="alert">
                                {{$errors->first('title')}}
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-3" for="email">Thứ tự hiển thị(*):</label>
                    <div class="col-sm-8">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-fw fa-file-text-o"></i></span>
                            <input type="number" class="form-control" name="stt" placeholder="Thứ tự hiển thị" value="{{ $menu['stt'] }}" required>
                        </div>
                        @if($errors->has('stt'))
                            <span class="text-center text-danger" role="alert">
                                {{$errors->first('stt')}}
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-8">
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-save"></i>Lưu
                        </button>
                        <a href="{{route('admin.menu.list')}}" type="submit" class="btn btn-danger">
                            <i class="fa fa-fw fa-close"></i>Hủy Bỏ
                        </a>
                    </div>
                </div>
            </form>

        </section>
        <!-- /.content -->
    </div>
@endsection

@section('javascript')
    {{----}}
    <script>
        $(document).ready(function () {
            $('.alert-dismissable').delay(5000).slideUp();
        });
    </script>
@endsection