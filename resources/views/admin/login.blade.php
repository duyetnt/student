<!doctype html>
<html lang="en">

<head>
    <title>Đăng nhập</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
        crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('public/admin')}}/login/css/main.css" />
</head>

<body>

    

    <div class="container" id="main">
            @if (session('error'))
            <div class="alert alert-danger hidden_h">
                    <strong>Danger!</strong>{{ session('error') }}
            </div>
            
        
            @endif


            <img src="{{asset('public/admin/login')}}/image/logo.png"  class="img1"/>
            <img src="{{asset('public/admin/login')}}/image/ten.png" class="img2"/>
            <img src="{{asset('public/admin/login')}}/image/line.png" class="img3"/>
            <img src="{{asset('public/admin/login')}}/image/magic.png" class="img4">
            <div class="hotline">
                    Hotline: (+84) 835 303 000 • Contact@vifonic.vn  
            </div>
        <div class="row torong" >

            <div class=" col-md-6 col-sm-6 col-6 main1" >
                <div class="main11">
                  
                    
                   
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-6 main2" style="font-size: 13px">
                   <p><b>Điền thông tin đăng nhập<br/>Tài khoản được Vifonic cung cấp</b></p> 
                   <form action="{{ route('admin.post.login') }}" method="post">
                       <div class="inp">
                           <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <input type="text" name="email" placeholder="enter email" class="input"/>
                       </div>
                       <div class="inp">
                      <input type="password" name="password" placeholder="PassWord" class="input"/>
                     </div>
                      <div class="forgot"><b><a href="#" style="color:black;margin-left: -12px;">Forgot your password?</a></b></div>
                       
                      <button type="submit" name="submit" value="ĐĂNG NHẬP" class="submit">ĐĂNG NHẬP</button>
                   </form>


                  <div class="connect"><b>Connect with us</b>
                    <ul >
                        <li><a href="https://www.facebook.com/vifonic.vn"><img src="{{asset('public/admin/login')}}/image/face.png" alt=""></a></li>
                        <li><a href="https://www.facebook.com/messages/t/vifonic.vn"><img src="{{asset('public/admin/login')}}/image/mess.png" alt=""></a></li>
                        <li><a href="https://zalo.me/0984062393"><img src="{{asset('public/admin/login')}}/image/zalo.png" alt=""></a></li>
                    </ul>

                    Privacy Policy
                
                </div>
            </div>
        </div>

        
    </div>

    

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script>
            $(document).ready(function(){
                $('.hidden_h').delay(5000).slideUp();
            });
    </script>

    {{-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script> --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>


   
</body>

</html>