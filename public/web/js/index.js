 function openNav() {
    $('#menu-mobile').css({
        transform: 'translateX(0)'
    });
    $('#menu-mobile').css({
        transition: 'all 0.5s'
    });
    $('body').css({
        overflow: 'hidden'
    });
    $('.wrapper').css({
    	transform: 'translateX(300px)',
    	transition: 'all 0.5s',
        background: 'rgba(0,0,0,0.8)'
    })
    $('.header-main').css({
        display:'none',
         transition: 'all 0.5s'
    });
    $(document).ready(function() {
        $('#rotation').addClass('rotation');
        $('#rotation').removeClass('rotation_backwards');
    });
}

function closeNav() {
    $('#menu-mobile').css({
        transform: 'translateX(-300px)'
    });
    $('#menu-mobile').css({
        transition: 'all 0.5s'
    });
    $('body').css({
        overflow: 'visible'
    });
    $('.header-main').css({
        display:'block',
         transition: 'all 0.5s'
    });
    $('.wrapper').css({
    	transform: 'translateX(0px)',
    	transition: 'all 0.5s',
    	background: 'none'
    })
    $(document).ready(function() {
        $('#rotation').addClass(' rotation_backwards');
        $('#rotation').removeClass('rotation');
    });
}

$(document).ready(function() {
	var ccc = $('#menu-mobile ul li').find('ul');
    if (ccc.length !== 0) {
        ccc.before('<button class="accordion"></button>');
        ccc.addClass('sub-menu');
    }
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].onclick = function() {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.maxHeight) {
                panel.style.maxHeight = null;
            } else {
                panel.style.maxHeight = panel.scrollHeight + 1000 + "px";
            }
        }
    }
  $('.banner .text .play').on('click', function(ev) {
    $("#video").trigger('play');
    $('.banner .text').css({
    	display: 'none'
    });
    $('.play').css({
    	display: 'none'
    });
  });
  $('.banner video').on('click', function(ev) {
    $("#video").trigger('pause');
    $('.play').css({
    	display: 'block'
    });
     $('.banner .text').css({
    	display: 'block'
    });
  });
  
  $('.slide_img_home').owlCarousel({
        loop:true,
        margin:0,
        dots:false,
        nav:true,
        autoplay:true,
        autoplaySpeed: 2000,
        autoTimeout: 4000,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:2
            },
            1000:{
                items:3
            }
        }
    });



    // about us
    $('#menu li').click(function(){
        $('#menu li.active').removeClass('active').addClass('hover');
        $(this).addClass('active').removeClass('hover');
    });



});



