<?php
/**
 * Created by PhpStorm.
 * User: Windows 10 TIMT
 * Date: 10/27/2018
 * Time: 8:55 AM
 */
define('CHECK_FILE', "true");
define('GEMINI_GEM', 1000);
define('CONTENT_LOAD_CODE_GEM', 'nap gem thong qua the nap gem');
define('CONTENT_LOAD_VNPAY_GEM', 'nap gem thong qua vnpay');
define('ACCESS_TOKEN', 'JHTHWPCE6OCZBW0PBH9XRRBC6JTR1UWQ');
//check file is file image
function checkFileImage($file)
{
    $extenxion = ['jpg', 'jpeg', 'png'];
    $check_file = "true";
    $imageFileType = strtolower(pathinfo($file->getClientOriginalName(),PATHINFO_EXTENSION));
    if (in_array($imageFileType, $extenxion))
    {
        return $check_file;
    }
}
// function: upload image to foder
function uploadFile($file)
{
    $full_url =  url()->current();
    $http = parse_url($full_url, PHP_URL_SCHEME);
    $host_name = parse_url($full_url, PHP_URL_HOST);
    $host = $http.'://'.$host_name;
    $images = '';
    if (!empty($file))
    {
        $url = "/upload/images/";
        $filename = time().'_'.$file->getClientOriginalName();
        $destinationPath = public_path($url);
        $file->move($destinationPath, $filename);
        $images = $host.'/public'.$url.$filename;
    }
    return $images;
}
// function delete file
function deleteFile($param){
    $array = explode('/',$param);
    unset($array[0],$array[1],$array[2], $array[3]);
    $data =  implode( "/", $array ); // change array to string
    \File::delete(public_path($data));
    return;
}
/**
 * hàm loại bỏ tất cả các ký tự đặc biệt
 * chỉ lấy ra chũ só
 **/
function getPriceFormat($price)
{
    $price_format = preg_replace("/[^0-9]/", '', $price);
    return $price_format;
}

function asset_url($param)
{
    return "/public". $param;
}

function setCodeGem($money = null)
{
    $data = [
        '10000' => 11,
        '20000' => 12,
        '50000' => 15,
        '100000' => 21,
        '200000' => 22,
    ];
    return $data[$money];
}


if (!function_exists('http_response_code')) {
    function http_response_code($code = NULL) {

        if ($code !== NULL) {

            switch ($code) {
                case 100: $text = 'Continue'; break;
                case 101: $text = 'Switching Protocols'; break;
                case 200: $text = 'OK'; break;
                case 201: $text = 'Created'; break;
                case 202: $text = 'Accepted'; break;
                case 203: $text = 'Non-Authoritative Information'; break;
                case 204: $text = 'No Content'; break;
                case 205: $text = 'Reset Content'; break;
                case 206: $text = 'Partial Content'; break;
                case 300: $text = 'Multiple Choices'; break;
                case 301: $text = 'Moved Permanently'; break;
                case 302: $text = 'Moved Temporarily'; break;
                case 303: $text = 'See Other'; break;
                case 304: $text = 'Not Modified'; break;
                case 305: $text = 'Use Proxy'; break;
                case 400: $text = 'Bad Request'; break;
                case 401: $text = 'Unauthorized'; break;
                case 402: $text = 'Payment Required'; break;
                case 403: $text = 'Forbidden'; break;
                case 404: $text = 'Not Found'; break;
                case 405: $text = 'Method Not Allowed'; break;
                case 406: $text = 'Not Acceptable'; break;
                case 407: $text = 'Proxy Authentication Required'; break;
                case 408: $text = 'Request Time-out'; break;
                case 409: $text = 'Conflict'; break;
                case 410: $text = 'Gone'; break;
                case 411: $text = 'Length Required'; break;
                case 412: $text = 'Precondition Failed'; break;
                case 413: $text = 'Request Entity Too Large'; break;
                case 414: $text = 'Request-URI Too Large'; break;
                case 415: $text = 'Unsupported Media Type'; break;
                case 500: $text = 'Internal Server Error'; break;
                case 501: $text = 'Not Implemented'; break;
                case 502: $text = 'Bad Gateway'; break;
                case 503: $text = 'Service Unavailable'; break;
                case 504: $text = 'Gateway Time-out'; break;
                case 505: $text = 'HTTP Version not supported'; break;
                default:
                    exit('Unknown http status code "' . htmlentities($code) . '"');
                    break;
            }

            $protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0');

            header($protocol . ' ' . $code . ' ' . $text);

            $GLOBALS['http_response_code'] = $code;

        } else {

            $code = (isset($GLOBALS['http_response_code']) ? $GLOBALS['http_response_code'] : 200);

        }

        return $code;

    }
}

function unparse_url($parsed_url) {
    $scheme   = isset($parsed_url['scheme']) ? $parsed_url['scheme'] . '://' : '';
    $host     = isset($parsed_url['host']) ? $parsed_url['host'] : '';
    $port     = isset($parsed_url['port']) ? ':' . $parsed_url['port'] : '';
    $user     = isset($parsed_url['user']) ? $parsed_url['user'] : '';
    $pass     = isset($parsed_url['pass']) ? ':' . $parsed_url['pass']  : '';
    $pass     = ($user || $pass) ? "$pass@" : '';
    $path     = isset($parsed_url['path']) ? $parsed_url['path'] : '';
    $query    = isset($parsed_url['query']) ? '?' . $parsed_url['query'] : '';
    $fragment = isset($parsed_url['fragment']) ? '#' . $parsed_url['fragment'] : '';
    return "$scheme$user$pass$host$port$path$query$fragment";
}