<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\NetWork;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mockery\Exception;

class NetWorkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $netWorks = NetWork::orderBy('id', 'desc')->get()->toArray();
        return view('admin.netWork.index', compact('netWorks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.netWork.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $check_icon = checkFileImage($request->icon);
        $check_image = checkFileImage($request->image);
        if ($check_icon == CHECK_FILE && $check_image == CHECK_FILE)
        {
            $icon = uploadFile($request->file('icon'));
            $image = uploadFile($request->file('image'));
        }else{
            return redirect()->route('admin.network.create')->with(['error' => "File bạn chọn không phải là file ảnh"]);
        }
        $inserts = new NetWork();
        $inserts->name = $request->name;
        $inserts->link = $request->link;
        $inserts->icon = $icon;
        $inserts->image = $image;
        try{
            $inserts->save();
            return redirect()->route('admin.network.list')->with(['success' => 'Thêm mạng xã hội thành công']);
        }catch (Exception $message){
            return redirect()->route('admin.network.list')->with(['error' => $message->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $netWork = NetWork::find($id);
        return view('admin.netWork.edit', compact('netWork'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $file_image = '';
        if (!empty($request->image)){
            $check_file = checkFileImage($request->image);
            if ($check_file == CHECK_FILE)
            {
                $file_image = uploadFile($request->file('image'));
                $get_image = NetWork::find($id)['image'];
                deleteFile($get_image);
            }else{
                return redirect()->back()->with(['error' => "File bạn chọn không phải là file ảnh"]);
            }
        }else{
            $file_image = $request->image_update;
        }
        // check icon
        if (!empty($request->icon)){
            $check_icon = checkFileImage($request->icon);
            if ($check_icon == CHECK_FILE)
            {
                $file_icon = uploadFile($request->file('icon'));
                $get_icon = NetWork::find($id)['icon'];
                deleteFile($get_icon);
            }else{
                return redirect()->back()->with(['error' => "File bạn chọn không phải là file ảnh"]);
            }
        }else{
            $file_icon = $request->image_icon;
        }
        $update = NetWork::findOrFail($id);
        $update->name = $request->name;
        $update->link = $request->link;
        $update->icon = $file_icon;
        $update->image = $file_image;
        try{
            $update->save();
            return redirect()->route('admin.network.list')->with(['success' => 'Update mang xa hoi thanh cong']);
        }catch (Exception $message){
            return redirect()->route('admin.network.list')->with(['error' => $message->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
