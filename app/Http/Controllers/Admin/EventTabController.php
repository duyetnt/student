<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\EventTab;
use App\Models\Admin\EventTranslation;
use App\Models\Admin\Language;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class EventTabController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $eventTabs = EventTab::orderBy('id', 'desc')->get()->toArray();
        return view('admin.eventTab.index', compact('eventTabs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.eventTab.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name_vi' => 'required|unique:event_tabs|max:225',
            'name_en' => 'required|unique:event_tabs|max:225',
            'title_vi' => 'required|max:255',
            'title_en' => 'required|max:255',
            'image' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput($request->all());
        }
        $file_image = '';
        $check_file = checkFileImage($request->image);
        if ($check_file == CHECK_FILE)
        {
            $file_image = uploadFile($request->file('image'));
        }else{
            return redirect()->back()->with(['error' => "File bạn chọn không phải là file ảnh"]);
        }

        // insert data to tablle post translation
        $inserts = new EventTab();
        $inserts->menu_id = 8;
        $inserts->name_vi = $request->name_vi;
        $inserts->name_en = $request->name_en;
        $inserts->title_vi = $request->title_vi;
        $inserts->title_en = $request->title_en;
        $inserts->image = $file_image;
        try{
            $inserts->save();
            return redirect()->route('admin.tab_event.list')->with(['success' => 'Them bai viet thanh cong']);
        }catch (Exception $message){
            return redirect()->route('admin.tab_event.list')->with(['error' => $message->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $eventTab = EventTab::find($id);
        return view('admin.eventTab.edit', compact('eventTab'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $file_image = '';
        if (!empty($request->image)){
            $check_file = checkFileImage($request->image);
            if ($check_file == CHECK_FILE)
            {
                $file_image = uploadFile($request->file('image'));
                $get_image = EventTab::find($id)['image'];
                deleteFile($get_image);
            }else{
                return redirect()->route('admin.post.create')->with(['error' => "File bạn chọn không phải là file ảnh"]);
            }
        }else{
            $file_image = $request->update_image;
        }
        $update = EventTab::findOrFail($id);
        $update->menu_id = 8;
        $update->name_vi = $request->name_vi;
        $update->name_en = $request->name_en;
        $update->title_vi = $request->title_vi;
        $update->title_en = $request->title_en;
        $update->image = $file_image;
        try{
            $update->save();
            return redirect()->route('admin.tab_event.list')->with(['success' => 'Update bai viet thanh cong']);
        }catch (Exception $message){
            return redirect()->route('admin.tab_event.list')->with(['error' => $message->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $get_image = EventTranslation::where('event_tab_id', $id)->get()->toArray();
        if (count($get_image) > 0){
            foreach ($get_image as $item) {
                deleteFile($item['image']);
            }
        }
        $get_image = EventTab::where('id', $id)->value('image');
        deleteFile($get_image);
        EventTranslation::where('event_tab_id', $id)->delete();
        EventTab::where('id', $id)->delete();
        return redirect()->back()->with(['success' => 'xoa thanh cong']);
    }
}
