<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\Event;
use App\Models\Admin\EventTranslation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Event::with('eventTranslations')->get()->toArray();
        //dd($events);
        return view('admin.event.index', compact('events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.event.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:event_translations|max:225',
            'simulation' => 'required|max:500',
            'image' => 'required',
            'content' => 'required',
            'language_id' => 'required',
            'menu_id' => 'required',
            'event_tab_id' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput($request->all());
        }
        $file_image = '';
        $check_file = checkFileImage($request->image);
        if ($check_file == CHECK_FILE)
        {
            $file_image = uploadFile($request->file('image'));
        }else{
            return redirect()->back()->with(['error' => "File bạn chọn không phải là file ảnh"]);
        }
        // insert data to table
        $events = new Event();
        try{
            $events->save();
        }catch (Exception $message){
            echo $message->getMessage();
        }
        // insert data to tablle post translation
        $inserts = new EventTranslation();
        $inserts->language_id = $request->language_id;
        $inserts->menu_id = $request->menu_id;
        $inserts->event_tab_id = $request->event_tab_id;
        $inserts->event_id = $events->id;
        $inserts->name = $request->name;
        $inserts->simulation = $request->simulation;
        $inserts->content = $request['content'];
        $inserts->image = $file_image;
        try{
            $inserts->save();
            return redirect()->route('admin.event.list')->with(['success' => 'Them bai viet thanh cong']);
        }catch (Exception $message){
            return redirect()->route('admin.event.list')->with(['error' => $message->getMessage()]);
        }
    }

    /**
     * @param $id_post
     * @param $menu_id
     * @param $language
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createPost($event_id, $event_tab_id ,$menu_id,$language){
        return view('admin.event._create', compact('event_id','menu_id','language', 'event_tab_id'));
    }

    /**
     * @param Request $request
     */
    public function storeLanguagePost(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:event_translations|max:225',
            'simulation' => 'required|max:500',
            'image' => 'required',
            'content' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput($request->all());
        }
        $file_image = '';
        $check_file = checkFileImage($request->image);
        if ($check_file == CHECK_FILE)
        {
            $file_image = uploadFile($request->file('image'));
        }else{
            return redirect()->back()->with(['error' => "File bạn chọn không phải là file ảnh"]);
        }

        // insert data to tablle post translation
        $request->language_id == 'english' ? $language = 'en' : $language = 'vi';
        $inserts = new EventTranslation();
        $inserts->menu_id = $request->menu_id;
        $inserts->event_id = $request->event_id;
        $inserts->event_tab_id = $request->event_tab_id;
        $inserts->language_id = $language;
        $inserts->name = $request->name;
        $inserts->simulation = $request->simulation;
        $inserts->content = $request['content'];
        $inserts->image = $file_image;
        try{
            $inserts->save();
            return redirect()->route('admin.event.list')->with(['success' => 'Them sự kiện thanh cong']);
        }catch (Exception $message){
            return redirect()->route('admin.event.list')->with(['error' => $message->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = EventTranslation::find($id);
        return view('admin.event.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $file_image = '';
        if (!empty($request->image)){
            $check_file = checkFileImage($request->image);
            if ($check_file == CHECK_FILE)
            {
                $file_image = uploadFile($request->file('image'));
                $get_image = EventTranslation::find($id)['image'];
                deleteFile($get_image);
            }else{
                return redirect()->back()->with(['error' => "File bạn chọn không phải là file ảnh"]);
            }
        }else{
            $file_image = $request->update_image;
        }

        $update = EventTranslation::findOrFail($id);
        $update->name = $request->name;
        $update->simulation = $request->simulation;
        $update->content = $request['content'];
        $update->image = $file_image;
        try{
            $update->save();
            return redirect()->route('admin.event.list')->with(['success' => 'Update sự kiện thanh cong']);
        }catch (Exception $message){
            return redirect()->route('admin.event.list')->with(['error' => $message->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $get_image = EventTranslation::where('event_id', $id)->get()->toArray();
        if (count($get_image) > 0){
            foreach ($get_image as $item) {
                deleteFile($item['image']);
            }
        }
        EventTranslation::where('event_id', $id)->delete();
        Event::where('id', $id)->delete();
        return redirect()->back()->with(['success' => 'xoa thanh cong']);
    }
}
