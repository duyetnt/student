<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\Menu;
use App\Models\Admin\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\PostTranslation;
use Mockery\Exception;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::with('postTranslations')->get()->toArray();
        return view('admin.post.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.post.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:post_translations|max:225',
            'simulation' => 'required|max:500',
            'image' => 'required',
            'content' => 'required',
            'language_id' => 'required',
            'menu_id' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('admin.post.create')
                ->withErrors($validator)
                ->withInput($request->all());
        }
        $file_image = '';
        $check_file = checkFileImage($request->image);
        if ($check_file == CHECK_FILE)
        {
            $file_image = uploadFile($request->file('image'));
        }else{
            return redirect()->route('admin.post.create')->with(['error' => "File bạn chọn không phải là file ảnh"]);
        }
        // insert data to table
        $posts = new Post();
        try{
            $posts->save();
        }catch (Exception $message){
            echo $message->getMessage();
        }
        // insert data to tablle post translation
        $inserts = new PostTranslation();
        $inserts->menu_id = $request->menu_id;
        $inserts->post_id = $posts->id;
        $inserts->language_id = $request->language_id;
        $inserts->code = str_slug(Menu::where('id', $request->menu_id)->value('name'));
        $inserts->name = $request->name;
        $inserts->simulation = $request->simulation;
        $inserts->content = $request['content'];
        $inserts->image = $file_image;
        try{
            $inserts->save();
            return redirect()->route('admin.post.list')->with(['success' => 'Them bai viet thanh cong']);
        }catch (Exception $message){
            return redirect()->route('admin.post.list')->with(['error' => $message->getMessage()]);
        }
    }

    /**
     * @param $id_post
     * @param $menu_id
     * @param $language
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createPost($post_id ,$menu_id,$language){
        return view('admin.post._create', compact('post_id','menu_id','language'));
    }

    /**
     * @param Request $request
     */
    public function storeLanguagePost(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:post_translations|max:225',
            'simulation' => 'required|max:500',
            'image' => 'required',
            'content' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput($request->all());
        }
        $file_image = '';
        $check_file = checkFileImage($request->image);
        if ($check_file == CHECK_FILE)
        {
            $file_image = uploadFile($request->file('image'));
        }else{
            return redirect()->back()->with(['error' => "File bạn chọn không phải là file ảnh"]);
        }

        // insert data to tablle post translation
        $request->language_id == 'english' ? $language = 'en' : $language = 'vi';
        $inserts = new PostTranslation();
        $inserts->menu_id = $request->menu_id;
        $inserts->post_id = $request->post_id;
        $inserts->language_id = $language;
        $inserts->code = str_slug(Menu::where('id', $request->menu_id)->value('name'));
        $inserts->name = $request->name;
        $inserts->simulation = $request->simulation;
        $inserts->content = $request['content'];
        $inserts->image = $file_image;
        try{
            $inserts->save();
            return redirect()->route('admin.post.list')->with(['success' => 'Them bai viet thanh cong']);
        }catch (Exception $message){
            return redirect()->route('admin.post.list')->with(['error' => $message->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = PostTranslation::find($id);
        return view('admin.post.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $file_image = '';
        if (!empty($request->image)){
            $check_file = checkFileImage($request->image);
            if ($check_file == CHECK_FILE)
            {
                $file_image = uploadFile($request->file('image'));
                $get_image = PostTranslation::find($id)['image'];
                deleteFile($get_image);
            }else{
                return redirect()->back()->with(['error' => "File bạn chọn không phải là file ảnh"]);
            }
        }else{
            $file_image = $request->update_image;
        }

        $update = PostTranslation::findOrFail($id);
        $update->name = $request->name;
        $update->simulation = $request->simulation;
        $update->content = $request['content'];
        $update->image = $file_image;
        try{
            $update->save();
            return redirect()->route('admin.post.list')->with(['success' => 'Update bai viet thanh cong']);
        }catch (Exception $message){
            return redirect()->route('admin.post.list')->with(['error' => $message->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $get_image = PostTranslation::where('post_id', $id)->get()->toArray();
        if (count($get_image) > 0){
            foreach ($get_image as $item) {
                deleteFile($item['image']);
            }
        }
        PostTranslation::where('post_id', $id)->delete();
        Post::where('id', $id)->delete();
        return redirect()->back()->with(['success' => 'xoa thanh cong']);
    }
}
