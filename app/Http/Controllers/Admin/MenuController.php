<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\Menu;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Mockery\Exception;
use Illuminate\Support\Facades\Validator;
class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menus = Menu::orderBy('stt', 'asc')->get()->toArray();
        return view('admin.menu.index', compact('menus'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.menu.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:menus|max:255',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('admin.menu.create')
                ->withErrors($validator)
                ->withInput($request->all());
        }
        $inserts = new Menu();
        $inserts->name = $request->name;
        $inserts->title = $request->title;
        $inserts->stt = $request->stt;
        $inserts->code = str_slug($request->name);
        try{
            $inserts->save();
            return redirect()->route('admin.menu.list')->with(['success' => 'Them menu thanh cong']);
        }catch (Exception $message){
            return redirect()->route('admin.menu.list')->with(['success' => $message->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $menu = Menu::find($id);
        return view('admin.menu.edit', compact('menu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $count = Menu::where('name', $request->name)->where('id','!=', $id)->count();
        if ($count > 0){
            return redirect()->back()->with(['error' => 'Menu này đã tồn tại']);
        }
        $update = Menu::findOrFail($id);
        $update->name = $request->name;
        $update->title = $request->title;
        $update->stt = $request->stt;
        $update->code = str_slug($request->name);
        try{
            $update->save();
            Cache::forever('menu'.$id, str_slug($request->name));
            return redirect()->route('admin.menu.list')->with(['success' => 'Update menu thanh cong']);
        }catch (Exception $message){
            return redirect()->route('admin.menu.list')->with(['error' => $message->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
