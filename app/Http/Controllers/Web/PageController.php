<?php

namespace App\Http\Controllers\Web;

use App\Models\Admin\Event;
use App\Models\Admin\EventTab;
use App\Models\Admin\EventTranslation;
use App\Models\Admin\NetWork;
use App\Models\Admin\Post;
use App\Models\Admin\PostTranslation;
use App\Models\Admin\Menu;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class PageController extends Controller
{
    //
    public function getIndex()
    {
        $posts = PostTranslation::orderBy('id', 'desc')->limit(10)->get()->toArray();
        return view('web.page.home', compact('posts'));
    }


    public function getAbouts()
    {
        return view('web.page.about');
    }

    public function getdepartments()
    {
        return view('web.page.departments');
    }

    public function getdepartments_details()
    {
        return view('web.page.departments_details');
    }

    public function getliving()
    {
        return view('web.page.living');
    }

    public function getliving_details()
    {
        return view('web.page.living_details');
    }

    public function getnews_even()
    {
        return view('web.page.news_even');
    }

    public function getnews_even_details()
    {
        return view('web.page.news_even_details');
    }

    public function getpartnership()
    {
        return view('web.page.partnership');
    }

    public function getpartnership_details()
    {
        return view('web.page.partnership_details');
    }

    public function getpeople()
    {
        return view('web.page.people');
    }

    public function getpeople_details()
    {
        return view('web.page.people_details');
    }

    public function getprograms()
    {
        return view('web.page.programs');
    }

    public function getstuden_socical()
    {
        return view('web.page.studen_socical');
    }

    public function getstudent_service_education()
    {
        return view('web.page.student_service_education');
    }

    public function getstudent_service()
    {
        return view('web.page.student_service');
    }

    /**
     * @param $params
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     */
    public function getPostByMenu($params){
        $language_id = 'en';
        if (Session::has('set_language')) {
            $language_id = Session::get('set_language');
        }
        $array = explode('-',$params);
        $count = count($array);
        $menu_id = $array[$count-1];
        //array_pop($array); // remove element last
        //$data =  implode( "-", $array ); // change array to string
        $menu = Menu::find($menu_id);
        $posts = DB::table('posts')
            ->join('post_translations', 'posts.id', '=', 'post_translations.post_id')
            ->where('menu_id', $menu_id)
            ->where('language_id',$language_id)
            ->select('post_translations.*')
            ->get()->toArray();
        switch ($menu['type']){
            case 1:
                return view('web.page._about', compact('posts', 'menu'));
                break;
            case 2:
                return view('web.page.people', compact('posts', 'menu'));
                break;
            case 3:
                return view('web.page.departments', compact('posts', 'menu'));
                break;
            case 4:
                return view('web.page.partnership', compact('posts', 'menu'));
                break;
            case 5:
                return view('web.page.programs', compact('posts', 'menu'));
                break;
            case 6:
                $value = PostTranslation::where('menu_id',$menu_id)
                    ->orderBy('id', 'desc')
                    ->where('language_id', $language_id)
                    ->first();
                return view('web.page.student_service', compact('posts', 'menu', 'value'));
                break;
            case 7:
                $tabEvents = EventTab::with('eventTranslations')->get()->toArray();
                //dd($tabEvents);
                return view('web.page.news_even', compact('tabEvents', 'menu', 'language_id'));
                break;
            case 8:
                return view('web.page.living', compact('posts', 'menu'));
                break;
            case 9:
                $networks = NetWork::get()->toArray();
                return view('web.page.studen_socical', compact('networks', 'menu'));
                break;
            default:return;
        }
    }

    /**
     * @param $menu
     * @param $params
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getPostById($menu_menu ,$params){
        $language_id = 'en';
        //check if isset session language
        if (Session::has('set_language')) {
            $language_id = Session::get('set_language');
        }
        // get post_trans_id from params
        $array = explode('-',$params);
        $count = count($array);
        $post_trans_id = $array[$count-1];

        // get menu by post_translation
        $menu = PostTranslation::find($post_trans_id)->menus;

        $post_id = PostTranslation::find($post_trans_id)->posts->id;
        $post_trans = Post::find($post_id)->postTranslations;
        $post = null;
        foreach ($post_trans as $post_tran) {
            if ($post_tran->language_id == $language_id){
                $post = $post_tran;
            }
        }

        // get all post of menu_id and language_id
        $posts = DB::table('posts')
            ->join('post_translations', 'posts.id', '=', 'post_translations.post_id')
            ->where('menu_id', $menu['id'])
            //->join('languages', 'post_translations.language_id', '=', 'languages.language_id')
            ->where('language_id',$language_id)
            ->select('post_translations.*')
            ->get()->toArray();

        return view('web.page._detail_post', compact('post','menu', 'posts'));
    }

    /**
     * @param $menu
     * @param $params
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getStudentServic($menu, $params){
        $language_id = 'en';
        if (Session::has('set_language')) {
            $language_id = Session::get('set_language');
            //dd($language_id);
        }
        $array = explode('-',$params);
        $count = count($array);
        $post_id = $array[$count-1];
        $menu = PostTranslation::find($post_id)->menus;
        $post = PostTranslation::where('language_id', $language_id)->where('id', $post_id)->first();
        //$posts = Menu::find($menu['id'])->posts; // get all post by the menus
        $posts = DB::table('posts')
            ->join('post_translations', 'posts.id', '=', 'post_translations.post_id')
            ->where('post_id', $post['post_id'])
            //->join('languages', 'post_translations.language_id', '=', 'languages.id')
            ->where('language_id',$language_id)
            ->select('post_translations.*')
            ->get()->toArray();
        return view('web.page.student_service', compact('post','menu', 'posts'));
    }

    public function getEventById($params){
        $language_id = 'en';
        if (Session::has('set_language')) {
            $language_id = Session::get('set_language');
            //dd($language_id);
        }
        $array = explode('-',$params);
        $count = count($array);
        $id = $array[$count-1];
        $event_id = EventTranslation::find($id)->events->id;
        $events = Event::find($event_id)->eventTranslations;
        $menu = EventTranslation::find($id)->menus;
        $tabEvents = EventTab::with('eventTranslations')->get()->toArray();
        //dd($tabEvents);

        $event = null;
        foreach ($events as $value) {
            if ($value->language_id == $language_id){
                $event = $value;
            }
        }

        return view('web.page.news_even_details', compact('event', 'tabEvents', 'menu', 'language_id'));

    }

    /**
     * @param $lang
     * @return \Illuminate\Http\RedirectResponse
     */
    public function translationLang($lang){
        if ($lang == 'vietnam'){
            Session::put('set_language', 'vi');
        }else{
            Session::put('set_language', 'en');
        }
        return redirect()->back();
    }
}
