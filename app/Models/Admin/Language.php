<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    protected $table = 'languages';
    protected $fillable = ['language_id','name','code'];
    protected $primaryKey = 'language_id';
    public $incrementing = false;
    public $timestamps = false;

    static function getLanguage(){
        $data = self::get()->toArray();
        foreach ($data as $item) {
            $value[$item['language_id']] = $item['code'];
        }
        return $value;
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function post(){
        return $this->hasMany(Post::class,'post_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function eventTabs(){
        return $this->hasMany(EventTab::class, 'language_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function events(){
        return $this->hasMany(Event::class, 'language_id');
    }

}
