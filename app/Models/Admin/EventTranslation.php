<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class EventTranslation extends Model
{
    protected $table = 'event_translations';
    protected $fillable = ['language_id', 'menu_id', 'event_tab_id', 'event_id', 'name', 'simulation', 'image', 'content'];

    static function getAllEvent(){
        $data = self::get()->toArray();
        $event=[];
        foreach ($data as $value) {
            $event['id'][$value['event_id']] = $value['id'];
            $event['name'][$value['event_id']] = $value['name'];
            $event['simulation'][$value['event_id']] = $value['simulation'];
            $event['menu'][$value['event_id']] = $value['menu_id'];
            $event['lang'][$value['event_id']] = $value['language_id'];
        }
        //dd($posts);
        return $event;
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function events(){
        return $this->belongsTo(Event::class,'event_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function menus(){
        return $this->belongsTo(Menu::class,'menu_id');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function eventTabs(){
        return $this->belongsTo(EventTab::class,'event_tab_id');
    }
}
