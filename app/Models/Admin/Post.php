<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'posts';
    protected $fillable = [];
    public $timestamps = false;
    /**
     * Get the postTranslations for the blog post.
     */
    public function postTranslations()
    {
        return $this->hasMany(PostTranslation::class, 'post_id');
    }
}
