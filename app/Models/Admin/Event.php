<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $table = 'events';
    protected $fillable = ['code'];
    public $timestamps = false;

    public function eventTranslations(){
        return $this->hasMany(EventTranslation::class, 'event_id');
    }
}
