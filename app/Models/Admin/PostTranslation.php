<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class PostTranslation extends Model
{
    protected $table = 'post_translations';
    protected $fillable = ['language_id', 'manu_id', 'post_id', 'name', 'image', 'simulation','content'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function menus(){
        return $this->belongsTo(Menu::class, 'menu_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function posts(){
        return $this->belongsTo(Post::class, 'post_id');
    }

    static function getAllPost(){
        $data = self::get()->toArray();
        $posts=[];
        foreach ($data as $value) {
            $posts['id'][$value['post_id']] = $value['id'];
            $posts['name'][$value['post_id']] = $value['name'];
            $posts['simulation'][$value['post_id']] = $value['simulation'];
            $posts['menu'][$value['post_id']] = $value['menu_id'];
            $posts['lang'][$value['post_id']] = $value['language_id'];
        }
        //dd($posts);
        return $posts;
    }
}
