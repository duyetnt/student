<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class EventTab extends Model
{
     protected $table = 'event_tabs';
     protected $fillable = ['language_id', 'menu_id','name_vi', 'title_vi','name_en', 'title_en', 'image'];

     static function getAllEventTabs(){
         $data = self::get()->toArray();
         $eventTab = null;
         foreach ($data as $items) {
             $eventTab['id'][$items['language_id']] = $items['id'];
             $eventTab['menu'][$items['language_id']] = $items['menu_id'];
             $eventTab['name'][$items['language_id']] = $items['name'];
             $eventTab['simulation'][$items['language_id']] = $items['simulation'];
             $eventTab['image'][$items['language_id']] = $items['image'];
         }
         return $eventTab;
     }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
     public function eventTranslations(){
         return $this->hasMany(EventTranslation::class, 'event_tab_id');
     }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
     public function menus(){
         return $this->belongsTo(Menu::class, 'menu_id');
     }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
     public function languages(){
         return $this->belongsTo(Language::class, 'language_id');
     }
}
