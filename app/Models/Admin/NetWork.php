<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class NetWork extends Model
{
    protected $table = 'networks';
    protected $fillable = ['name', 'link', 'icon', 'image'];
}
