<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = 'menus';
    protected $fillable = ['name', 'title', 'stt', 'code'];


    static function getAllMenu(){
        $data = self::orderBy('id', 'desc')->get()->toArray();
        $menu = [];
        if(count($data) > 0){
            foreach ($data as $value){
                $menu[$value['id']] = $value['name'];
            }
        }
        return $menu;
    }
    /**
     * get post for the menus
     */
    public function posts(){
        return $this->hasMany(PostTranslation::class, 'menu_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function eventTabs(){
        return $this->hasMany(EventTab::class, 'menu_id');
    }
}
