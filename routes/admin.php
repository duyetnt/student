<?php 

use Illuminate\Support\Facades\Route;


Route::get('/login', 'Admin\LoginController@getLogin')->name('admin.login');

// login admin
Route::post('/postlogin', 'Admin\LoginController@postLogin')->name('admin.post.login');


Route::group(['prefix' => 'admin','middleware' => 'auth'], function() {

    /**
     * Route group menu
     */
    Route::group(['prefix' => 'menu'], function(){
        Route::get('/list', 'Admin\MenuController@index')->name('admin.menu.list');
        Route::get('/create', 'Admin\MenuController@create')->name('admin.menu.create');
        Route::post('/store', 'Admin\MenuController@store')->name('admin.menu.store');
        Route::get('/edit/{id}', 'Admin\MenuController@edit')->name('admin.menu.edit');
        Route::post('/update/{id}', 'Admin\MenuController@update')->name('admin.menu.update');
        Route::post('/destroy/{id}', 'Admin\MenuController@destroy')->name('admin.menu.destroy');
    });

    /**
     * Route group language
     */
    Route::group(['prefix' => 'language'], function(){
        Route::get('/list', 'Admin\LanguageController@index')->name('admin.language.list');
        Route::get('/create', 'Admin\LanguageController@create')->name('admin.language.create');
        Route::post('/store', 'Admin\LanguageController@store')->name('admin.language.store');
        Route::get('/edit/{id}', 'Admin\LanguageController@edit')->name('admin.language.edit');
        Route::post('/update/{id}', 'Admin\LanguageController@update')->name('admin.language.update');
        Route::post('/destroy/{id}', 'Admin\LanguageController@destroy')->name('admin.language.destroy');
    });
    /**
     * Route group language
     */
    Route::group(['prefix' => 'post'], function(){
        Route::get('/list', 'Admin\PostController@index')->name('admin.post.list');
        Route::get('/create', 'Admin\PostController@create')->name('admin.post.create');
        Route::post('/store', 'Admin\PostController@store')->name('admin.post.store');
        Route::get('/edit/{id}', 'Admin\PostController@edit')->name('admin.post.edit');
        Route::post('/update/{id}', 'Admin\PostController@update')->name('admin.post.update');
        Route::get('/destroy/{id}', 'Admin\PostController@destroy')->name('admin.post.destroy');
        Route::post('/store-lang', 'Admin\PostController@storeLanguagePost')->name('admin.post.store.lang');
        Route::get('/create-lang/{post_id}/{menu_id}/{lang}', 'Admin\PostController@createPost')->name('admin.post.create.lang');
    });
    /**
     * Route group network
     */
    Route::group(['prefix' => 'network'], function(){
        Route::get('/list', 'Admin\NetWorkController@index')->name('admin.network.list');
        Route::get('/create', 'Admin\NetWorkController@create')->name('admin.network.create');
        Route::post('/store', 'Admin\NetWorkController@store')->name('admin.network.store');
        Route::get('/edit/{id}', 'Admin\NetWorkController@edit')->name('admin.network.edit');
        Route::post('/update/{id}', 'Admin\NetWorkController@update')->name('admin.network.update');
        Route::post('/destroy/{id}', 'Admin\NetWorkController@destroy')->name('admin.network.destroy');
    });
    /**
     * Route Tab Event
     */
    Route::group(['prefix' => 'tab_event'], function(){
        Route::get('/list', 'Admin\EventTabController@index')->name('admin.tab_event.list');
        Route::get('/create', 'Admin\EventTabController@create')->name('admin.tab_event.create');
        Route::post('/store', 'Admin\EventTabController@store')->name('admin.tab_event.store');
        Route::get('/edit/{id}', 'Admin\EventTabController@edit')->name('admin.tab_event.edit');
        Route::post('/update/{id}', 'Admin\EventTabController@update')->name('admin.tab_event.update');
        Route::get('/destroy/{id}', 'Admin\EventTabController@destroy')->name('admin.tab_event.destroy');
    });
    /**
     * Route Event
     */
    Route::group(['prefix' => 'event'], function(){
        Route::get('/list', 'Admin\EventController@index')->name('admin.event.list');
        Route::get('/create', 'Admin\EventController@create')->name('admin.event.create');
        Route::post('/store', 'Admin\EventController@store')->name('admin.event.store');
        Route::get('/edit/{id}', 'Admin\EventController@edit')->name('admin.event.edit');
        Route::post('/update/{id}', 'Admin\EventController@update')->name('admin.event.update');
        Route::get('/destroy/{id}', 'Admin\EventController@destroy')->name('admin.event.destroy');
        Route::post('/store-lang', 'Admin\EventController@storeLanguagePost')->name('admin.event.store.lang');
        Route::get('/create-lang/{event_id}/{event_tab_id}/{menu_id}/{lang}', 'Admin\EventController@createPost')->name('admin.event.create.lang');
    });


    Route::get('/', function () {
        return view('welcome');
    });
    
    Auth::routes();
    
    Route::get('/home', 'HomeController@index')->name('home');


    // caidat
    Route::get('/caidat', 'Admin\OptionController@getcaidat')->name('caidat');

// logout admin
    Route::get('/getlogout', 'Admin\LoginController@getLogout')->name('admin.logout');

 


});